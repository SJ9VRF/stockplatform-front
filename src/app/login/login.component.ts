import { Component, OnInit, Inject } from '@angular/core';
import{MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import{AuthService} from '../services/auth.service';
import{FormGroup, FormBuilder, Validators} from '@angular/forms'
import{TranslocoService} from '@ngneat/transloco';
import { from } from 'rxjs';

interface Data {
  username : string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  logInForm : FormGroup;
  user = {email : '' , password : ''}
  errMess : string;

  formErrors = {
      'email' : '',
      'password': ''
    };
    validationMessages = {
      'email' : { 
        'required': 'آدرس ایمیل الزامی است',
        'email' : 'فرمت ایمیل نادرست است'
      },
      'password' : {
        'required' : 'رمز عبور الزامی است'
      }
    }

  constructor(public dialog : MatDialogRef<LoginComponent>,
    @Inject(MAT_DIALOG_DATA) public data : Data, private translocoService : TranslocoService,
    private authservice : AuthService, private fb : FormBuilder) { }

  ngOnInit() {
    this.createForm();
  }

  createForm(){
    this.logInForm = this.fb.group({
       email : ['',[ Validators.required , Validators.email]],
       password : ['' , [Validators.required]]
     });   
     this.logInForm.valueChanges.subscribe((data) => this.onValueChanged(data));
  }

  onValueChanged(data : any){
    if(!this.logInForm){return ;}
    const form = this.logInForm;
    //clear previous err message
    for(const field in this.formErrors){
      if(this.formErrors.hasOwnProperty(field)){
        this.formErrors[field] = '';
        const control = form.get(field);
        //re check for erros
        if(control && control.dirty && !control.valid){
          const messages = this.validationMessages[field];
          console.log('control errors : ' ,control.errors);
          for(const key in control.errors){
            if(control.errors.hasOwnProperty(key)){
              console.log('key =' , key);
              console.log('message[key]', messages);
              this.formErrors[field] += this.translocoService.translate(`formErrors.${field}.${key}`) + '\t';
            }
          }
        }
      }
    }
  }
  onSubmitLogInForm(){
    this.user = this.logInForm.value;
    this.authservice.logIn(this.user).subscribe((result) => {
      console.log('result', result);
      this.dialog.close('Successful');
    }, (err)=>{
      this.errMess = 'رمز عبور یا آدرس ایمیل اشتباه است';
      console.log('err' , err);
    })
  }
}
