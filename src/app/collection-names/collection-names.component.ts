import { Component, OnInit , Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import{StrategyService} from '../services/strategy.service';


@Component({
  selector: 'app-collection-names',
  templateUrl: './collection-names.component.html',
  styleUrls: ['./collection-names.component.scss']
})
export class CollectionNamesComponent implements OnInit {

  constructor(public dialogRef : MatDialogRef<CollectionNamesComponent> ,
    @Inject(MAT_DIALOG_DATA) public data : string[], private strategyService : StrategyService) { }
    
    //this variable hold all collection names
    wholeCollectionNames : string[];
    //this variable hold the collection names that match with searchWord
    collectionNames : string[];
    selectedOptions : string[] = [];
    searchWord : string;

  ngOnInit() {
    //get stocks name
    this.strategyService.getCollectionNames().subscribe((colNames) => {
      this.collectionNames = colNames;
      this.wholeCollectionNames = this.collectionNames;
      console.log('collectionNames', this.collectionNames);
    })
  }
  onNgModelChange(event){
    console.log('on ng model change', event);
  }
  submitDialog(){
    console.log('selected', this.selectedOptions);
    this.dialogRef.close(this.selectedOptions);
  }

  closeDialog(){
    //this.dialogRef.close(this.selectedOptions);
  }

  searchThis(){
    this.collectionNames = this.wholeCollectionNames.filter((collection) => collection.includes(this.searchWord));
    
  }
}
