import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectionNamesComponent } from './collection-names.component';

describe('CollectionNamesComponent', () => {
  let component: CollectionNamesComponent;
  let fixture: ComponentFixture<CollectionNamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectionNamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionNamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
