import { Component, OnInit, OnDestroy } from '@angular/core';
import{ ActivatedRoute, Params, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import {NewsService} from '../services/news.service';
import {News} from '../shared/news';
import{baseurl} from '../shared/baseurl';
import {AuthService} from '../services/auth.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.scss']
})
export class NewsDetailComponent implements OnInit, OnDestroy {

  news : News;
  url :string =  baseurl;
  admin : boolean = false;
  isAdminSubscrip : Subscription;
  activeComment : boolean = false;
  activeCommentId : string = undefined;
  updatedContent : string = '';
  newContent : string = '';
  isAuth : boolean = false;
  email : string;

  constructor(private route : ActivatedRoute, private newsService : NewsService,
    private authService : AuthService, private router : Router) { }

  ngOnDestroy(): void {
    this.isAdminSubscrip.unsubscribe();
  }

  ngOnInit() {
    this.isAuth = this.authService.isLoggedIn();
    this.isAdminSubscrip = this.authService.getAdmin().subscribe((isAdmin) =>{ this.admin = isAdmin;
    console.log('isadmin', this.admin);});
    this.authService.loadUserCredentials();
    this.route.params.pipe(switchMap((params : Params) => {
      return this.newsService.getNewsById(params['newsId'])}))
      .subscribe((news) => {
          this.email = this.authService.getEmail();
          //check for comment owner to allow the owner to edit his comment
          for(let i=0 ; i< news.comments.length; i++ ){
            if(news.comments[i].author.email === this.email){
              news.comments[i]['owner']= true;
            }
            else{
              news.comments[i]['owner']= false;
            }
          }
          this.news = news;
          console.log('this.news', this.news);
      });
  }
  deleteNews(){
    this.newsService.deleteNewsById(this.news['_id']).subscribe((news) => {
      console.log('delete Successfylly');
      this.router.navigate(['/news']);
    });
  }
  //get the comment that user want to edit
  activeCommentArea(id : any, content: string){
    console.log('event', id);
    console.log('content', content);
    this.updatedContent= content;
    this.activeCommentId = id;
    this.activeComment = true;
  }
  //update specific comment in database
  updateNews(newsId : string, commntId : string){
    this.newsService.updateNewsComment(newsId, commntId, this.updatedContent).subscribe((news) => {
      this.news = news;
      console.log('updateComment', this.news);
      this.activeComment = false;
    })
  }
  //post new comment to database
  postComment(){
    this.newsService.postNewsComment(this.news._id , this.newContent).subscribe((news) => {
      this.email = this.authService.getEmail();
          for(let i=0 ; i< news.comments.length; i++ ){
            if(news.comments[i].author.email === this.email){
              news.comments[i]['owner']= true;
            }
            else{
              news.comments[i]['owner']= false;
            }
          }
      this.news = news;
      console.log('post new comment', this.news);
    })
  }
  deleteComment(newsId : string,  commentId : string){
    this.newsService.deleteCommentById(newsId, commentId ).subscribe((news) => {
      this.email = this.authService.getEmail();
      for(let i=0 ; i< news.comments.length; i++ ){
        if(news.comments[i].author.email === this.email){
          news.comments[i]['owner']= true;
        }
        else{
          news.comments[i]['owner']= false;
        }
      }
      this.news = news;
    })
  }
}
