import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewChild } from '@angular/core';
import {ProfileManagmentService} from '../services/profile-managment.service';
import{NewsService} from '../services/news.service';
import{News} from '../shared/news';
import{Subscription} from 'rxjs';
import{baseurl} from '../shared/baseurl';
import{AuthService} from '../services/auth.service';
import { Observable } from 'rxjs';
import { MatTableDataSource} from '@angular/material/table';
import{MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit, OnDestroy {

  admin:boolean =false;
  //this variable holds the news that match with searchWord
  news : News [] ;
  getNewsSubscrip : Subscription;
  url : string = baseurl;
  htmlContent : string = "<p>im here</p>";
  new : News;
  //this variable holds all news 
  wholeNews: News[];
  searchWord : string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  obs: Observable<any>;
  dataSource: MatTableDataSource<News>;


  constructor(private profileManagmentService : ProfileManagmentService,
    private newsService : NewsService, private authService : AuthService, private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit() {

    this.authService.getAdmin().subscribe((isAdmin) => {this.admin = isAdmin;})
    this.authService.loadUserCredentials();
    this.getNewsSubscrip = this.newsService.getNews().subscribe((news) => {
      this.news = news;
      this.wholeNews = this.news;
      this.dataSource  = new MatTableDataSource<News>(this.news);
      this.changeDetectorRef.detectChanges();
      this.dataSource.paginator = this.paginator;
      const paginatorIntl = this.paginator._intl;
      paginatorIntl.nextPageLabel = '';
      paginatorIntl.previousPageLabel = '';
      paginatorIntl.firstPageLabel ='';
      paginatorIntl.lastPageLabel = '';
      this.obs = this.dataSource.connect();
     console.log('news',news);
   });

  }

  ngOnDestroy(){
    this.getNewsSubscrip.unsubscribe();
    if (this.dataSource) { 
      this.dataSource.disconnect(); 
    }
  }
 
  searchThis(){
    this.news=  this.wholeNews .filter((news) => news.title.includes(this.searchWord) );
    this.dataSource =  new MatTableDataSource<News>(this.news);
    this.changeDetectorRef.detectChanges();
      this.dataSource.paginator = this.paginator;
      this.obs = this.dataSource.connect();
  }

}
