//this class  hold the functions and variables blong to each block
import {SMA, CrossDown, CrossUp, MACD, RSI, ADX, WilliamsR, ATR, MFI, CCI, OBV, BollingerBands,StochasticRSI
    ,IchimokuCloud, EMA, WMA, Stochastic} from 'technicalindicators';
//var tulind = require('tulind');

export class DiagramNode {
    //variables for all blocks
    id : string;
    name : string;
    NodeX : number;
    NodeY : number;
    value1 : any;
    value2 : any;
    type : any;
    input : any;
    timeframe : string = "Daily";
    values : number[];
    high : number [];
    low : number [];
    open : number [];
    close : number[];
    fastPeriod : number;
    slowPeriod : number;
    signalPeriod : number;
    SimpleMAOscillator: boolean;
    SimpleMASignal : boolean;
    period : number;
    stdDev : number;
    practicalPrice : string = "Close";
    volume : number[];
    isReady : boolean = false;
    kPeriod : number;
    dPeriod : number;
    stochasticPeriod : number;
    rsiPeriod : number;
    averageMethod : string = 'Simple';
    mathMethod : string="min";
    conversionPeriod : number;
    basePeriod : number;
    spanPeriod : number;
    displacement : number;
    comparisonValue1 : number;
    comparisonValue2 : number
    MAInput: any = {period : 10 , values: this.values};
    SMAInput2: any = {period : 12 , values: this.values};
    //Indicator input
    MACDInput = {
        values            : this.values,
        fastPeriod        : this.fastPeriod,
        slowPeriod        : this.slowPeriod,
        signalPeriod      : this.signalPeriod ,
        SimpleMAOscillator: false,
        SimpleMASignal    : false
      }
    ADXInput = {
          high : this.high,
          low : this.low,
          close : this.close,
          period : 14
    }
    WilliamsInput = {
        high : this.high,
        low : this.low,
        close : this.close,
        period : 14
    }
    ATRInput = {
        high : this.high,
        low : this.low,
        close : this.close,
        period : 14
    }
    
    MFIInput = {
        high : this.high,
        low : this.low,
        close : this.close,
        volume : this.volume,
        period : 14
      }
    CCIInput = {
        high : this.high,
        low : this.low,
        close : this.close,
        open : this.open,
        period : 14
    }
    OBVInput={
        close : this.close,
        volume : this.volume
    }
    BollingerInput={
        values : this.values,
        stdDev : this.stdDev,
        period : 14
    }
    RSIInput: any = {period : 10 , values: this.values};
    stochasticRSIInput={
        values : this.values,
        rsiPeriod : this.rsiPeriod,
        kPeriod : this.kPeriod,
        dPeriod : this.dPeriod,
        stochasticPeriod : this.stochasticPeriod,
    }

    IchimokuInput = {
        high : this.high,
        low : this.low,
        conversionPeriod : this.conversionPeriod,
        basePeriod : this.basePeriod,
        spanPeriod : this.spanPeriod,
        displacement : this.displacement
    }

    stochasticInput = {
        high : this.high,
        low : this.low,
        close : this.close,
        period : this.period,
        signalPeriod : this.signalPeriod
    }
    OutPut : string;
    constructor(options : {
        id ? : string,
        name ? : string,
        NodeX ?: number,
        NodeY? : number,
        value1 ? : any,
        value2? : any,
        type ? :any ,
        OutPut ? : string,
        period? : number,
        timeframe? : string,
        practicalPrice? : string,
        fastPeriod? : number,
        slowPeriod ? : number,
        signalPeriod ?: number,
        stdDev ?: number,
        kPeriod ?: number,
        dPeriod ?: number,
        stochasticPeriod ? : number,
        rsiPeriod ? : number,
        conversionPeriod ? : number,
        basePeriod? : number,
        spanPeriod? : number,
        displacement? : number,
        comparisonValue1 ? : number,
        comparisonValue2 ? : number,
    }){
        this.practicalPrice = options.practicalPrice? options.practicalPrice : 'Open';
        this.period = options.period? options.period : 14;
        this.timeframe = options.timeframe? options.timeframe : "Current" ;
        this.id = options.id;
        this.name = options.name;
        this.NodeY = options.NodeY;
        this.NodeX = options.NodeX;
        this.value1 = options.value1;
        this. value2 = options.value2;
        this.type = options.type;
        this.OutPut = options.OutPut;
        this.fastPeriod = options.fastPeriod;
        this.slowPeriod= options.slowPeriod;
        this.signalPeriod = options.signalPeriod;
        this.stdDev = options.stdDev;
        this.kPeriod = options.kPeriod;
        this.dPeriod = options.dPeriod;
        this.stochasticPeriod = options.stochasticPeriod;
        this.rsiPeriod = options.rsiPeriod;
        this.conversionPeriod = options.conversionPeriod;
        this.basePeriod = options.basePeriod;
        this.spanPeriod = options.spanPeriod;
        this.displacement = options.displacement;
        this.comparisonValue1 = options.comparisonValue1;
        this.comparisonValue2 = options.comparisonValue2;
    }
    //call block function with specific input
    blockFunction(): any{
        switch (this.name) {
            
            case 'Stochastic':
                this.stochasticInput.close = this.close;
                this.stochasticInput.high = this.high;
                this.stochasticInput.low = this.low;
                this.stochasticInput.period = Number(this.period);
                this.stochasticInput.signalPeriod = Number(this.signalPeriod);
                return Stochastic.calculate(this.stochasticInput);
                break;
            case 'commonMath':
                switch (this.mathMethod) {
                    case 'min':
                        return Math.min.apply(null,this.values);
                        break;
                    case 'max':
                        return Math.max.apply(null,this.values);
                        break;    
                    case 'average':
                        let average=0;
                        for(let value of this.values){
                            average += value;
                        }
                        return (average/this.values.length)
                        break;    
                    case 'sum':
                        let sum =0;
                        for(let value of this.values){
                            sum += value;
                        }
                        return sum;
                        break;    
                    case 'middle':
                        return this.middle(this.values);
                        break;
                    case 'standardDeviation':
                        return this.calculateStdDev(this.values);
                        break;
                    case 'variance':
                        return this.variance(this.values);
                        break;
                    default:
                        break;
                }
                break;
            case 'Ichimoku':
                this.IchimokuInput.basePeriod = this.basePeriod;
                this.IchimokuInput.conversionPeriod = this.conversionPeriod;
                this.IchimokuInput.spanPeriod = this.spanPeriod;
                this.IchimokuInput.displacement = this.displacement;
                this.IchimokuInput.low = this.low;
                this.IchimokuInput.high = this.high;
                let IchimokuResult = IchimokuCloud.calculate(this.IchimokuInput);
                switch (this.OutPut) {
                    case 'conversion':
                        return IchimokuResult.map(element => element.conversion);
                        break;
                    case 'base':
                        return IchimokuResult.map(element => element.base)
                        break;
                    case 'spanA':
                        return IchimokuResult.map(element => element.spanA);
                        break;
                    case 'spanB':
                        return IchimokuResult.map(element => element.spanB);
                        break;
                    default:
                        return IchimokuResult.map(element => element.conversion);
                        break;
                }
                break;
            case 'StochRsi':
                this.stochasticRSIInput.dPeriod = Number(this.dPeriod);
                this.stochasticRSIInput.kPeriod = Number(this.kPeriod);
                this.stochasticRSIInput.rsiPeriod = Number(this.rsiPeriod);
                this.stochasticRSIInput.stochasticPeriod = Number(this.stochasticPeriod);
                this.stochasticRSIInput.values = this.values;
                let stochasticRSIResult = StochasticRSI.calculate(this.stochasticRSIInput);
                switch (this.OutPut) {
                    case 'd':
                        return stochasticRSIResult.map(element => element.d);
                        break;
                    case 'k':
                        return stochasticRSIResult.map(element => element.k)
                        break;
                    case 'stochRSI':
                        return stochasticRSIResult.map(element => element.stochRSI);
                        break;
                    default:
                        return stochasticRSIResult.map(element => element.stochRSI);
                        break;
                }
            break;
            case 'BollingerBands':
                this.BollingerInput.values = this.values;
                this.BollingerInput.stdDev = Number(this.stdDev);
                this.BollingerInput.period = Number(this.period);
                let bollingerResult = BollingerBands.calculate(this.BollingerInput);
                switch (this.OutPut) {
                    case 'Middle':
                        return bollingerResult.map(element => element.middle);
                        break;
                    case 'Lower':
                        return bollingerResult.map(element => element.lower)
                        break;
                    case 'Upper':
                        return bollingerResult.map(element => element.upper);
                        break;
                    default:
                        return bollingerResult.map(element => element.middle);
                        break;
                }

            case 'OBV':
                this.OBVInput.close = this.close;
                this.OBVInput.volume = this.volume;
                return OBV.calculate(this.OBVInput);
                break;
            case 'CCI':
                this.CCIInput.close = this.close;
                this.CCIInput.high = this.high;
                this.CCIInput.low = this.low;
                this.CCIInput.period = Number(this.period);;
                this.CCIInput.open = this.open;
                return CCI.calculate(this.CCIInput);
                break;
            case 'MFI':
                this.MFIInput.close = this.close;
                this.MFIInput.high = this.high;
                this.MFIInput.low = this.low;
                this.MFIInput.period =Number(this.period);
                this.MFIInput.volume = this.volume;
                return MFI.calculate(this.MFIInput);
                break;
            
            case 'Williams %R':
                this.WilliamsInput.close = this.close;
                this.WilliamsInput.high = this.high;
                this.WilliamsInput.low = this.low;
                this.WilliamsInput.period = Number(this.period);
                return WilliamsR.calculate(this.WilliamsInput);
                break;
            case 'ATR':
                this.ATRInput.close = this.close;
                this.ATRInput.high = this.high;
                this.ATRInput.low = this.low;
                this.ATRInput.period = this.period;
                return ATR.calculate(this.ATRInput);
                 break;
            case 'ADX' :
                this.ADXInput.close = this.close;
                this.ADXInput.high = this.high;
                this.ADXInput.low = this.low;
                this.ADXInput.period = this.period;
                let ADXResult = ADX.calculate(this.ADXInput)
                switch (this.OutPut) {
                    case 'Adx':
                        return ADXResult.map(element => element.adx);
                        break;
                    case '+DI':
                        return ADXResult.map(element => element.mdi)
                        break;
                    case '-DI':
                        return ADXResult.map(element => element.pdi);
                        break;
                    default:
                        return ADXResult.map(element => element.adx);
                        break;
                }
            case 'RSI':
                this.RSIInput.values = this.values;
                this.RSIInput.period = this.period;
                return RSI.calculate(this.RSIInput);
                break;
            case 'MA':
                this.MAInput.values = this.values;
                this.MAInput.period = this.period;
                switch (this.averageMethod) {
                    case 'Simple':
                        /*tulind.indicators.sma.indicator([1,2,3,4,5,6], [3], function(err, results) {
                            console.log('tulindResult',result);
                        })*/
                        return SMA.calculate(this.MAInput);
                        break;
                    case 'LinearWeighted':
                        return WMA.calculate(this.MAInput);
                        break;
                    case 'Exponential':
                        return EMA.calculate(this.MAInput)
                        break;    
                    default:
                        break;
                }
                
                break;
            case 'MACD':
                this.MACDInput.fastPeriod = this.fastPeriod;
                this.MACDInput.slowPeriod = this.slowPeriod;
                this.MACDInput.values = this.values;
                this.MACDInput.signalPeriod = this.signalPeriod;
                let result = MACD.calculate(this.MACDInput);
                switch (this.OutPut) {
                    case 'Macd':
                        return result.map(element => element.MACD);
                        break;
                    case 'Signal':
                        return result.map(element => element.signal)
                        break;
                    case 'Histogram':
                        return result.map(element => element.histogram);
                        break;
                    default:
                        return result.map(element => element.MACD);
                        break;
                }
            case 'Comparison':
                this.isReady = true;
                return this.comparison(this.comparisonValue1, this.comparisonValue2, this.type);
            break;

            case 'And':
                return this._And(this.value1,this.value2);
                break;

            case 'Or':
                return this._Or(this.value1, this.value2);
                break;

            case 'Cross':                    
                if(this.value1.isReady && this.value2.isReady){ this.isReady = true;
                    let result = this.cross(this.value1.blockFunction(), this.value2.blockFunction(),this.type); 
                    console.log('Crossrun value1= ',this.value1.name,'function', this.value1.blockFunction() );
                    console.log('Cross run value2= ',this.value2.name,'function', this.value2.blockFunction());
                    console.log('Cross Result', result);
                    return result; 
                }               
                break; 
            case 'PriceData':
                return this.values;
            default:
                break;
            case 'Signal':
                return this.value1.blockFunction();
                break;
        }
    }
    //decide the block is raedy to run its function or not
    setIsReady(){
        switch (this.name) {
            case 'Cross':
                if(this.value1.isReady && this.value2.isReady){
                    this.isReady= true
                }
                return this.isReady;
                break;
            case 'Comparison':
                if(this.comparisonValue1 && this.comparisonValue2){
                    this.isReady = true;
                } 
                return this.isReady;
                break;
            case 'And':
                if(this.value1 && this.value2){
                    this.isReady = true;
                } 
                return this.isReady;
                break;   
            case 'Or':
                if(this.value1 && this.value2){
                    this.isReady = true;
                } 
                return this.isReady;
                break;    
            default:
                break;
        }
    }
    comparison(value1 : any, value2 : any, type: string){
        value1 = Number(value1);
        value2 = Number(value2);
        switch (type) {
            case '>':
                this.isReady=true;
                return value1 > value2;
                break;

            case '<':
                this.isReady=true;
                return value1 < value2
                break;
            
            case '>=':
                this.isReady=true;
                return value1 >= value2;
                break;

            case '<=':
                this.isReady=true;
                return value1 <= value2;
                break;
            
            case '=':
                this.isReady=true;
                return value1===value2;
                break;

            default:
                console.log('defualt');
                break;
        }
    }
    _And(value1 : any , value2 : any){
        let result1 = this.value1.blockFunction();
        let result2 = this.value2.blockFunction();
        let res1 = false;
        let res2 = false;
        let result = false;
        let prices : number[] = [];
        console.log('result1', result1, 'result2', result2);
        if(result1 instanceof Array){
            console.log('result1 is instance of arrray', result1.indexOf(true))
            if(result1.indexOf(true) !== -1){
                let j =0;
                for(let i=0; i < result1.length; i++){
                    if(result1[i]){
                      prices[j] = this.value1.value1.values[i];
                      j++;
                    }
                  }
                res1 = true;
            }
            else{
                res1 = false;
            }
        }
        else{
            res1 = result1;
        }
        if(result2 instanceof Array){
            if(result2.indexOf(true) !== -1){
                let j=0;
                for(let i=0; i < result1.length; i++){
                    if(result1[i]){
                      prices[j] = this.value2.value1.values[i];
                      j++;  
                    }
                }
                res2 = true;
            }
            else{
                res2 = false;
            }
        }
        else{
            res2 = result2;
        }
        if(res1 && res2){
            result = true;
            console.log('resultAnd',result);
        }
        else{
            result = false;
            console.log('resultAnd',result);
        }
        return {result: result , prices : prices};
    }
    _Or(value1 : any , value2 : any){
        let result1 = value1.blockFunction();
        let result2 = value2.blockFunction();
        let result = false;
        let price;
        if(result1 instanceof Array){
            if(result1.indexOf(true) !== -1){
                price =this.value1.value1.values[result1.indexOf(true)];
                result = true
            }
        }
        else{
            if(result1){
               result = true;
            }
        }
        if(result2 instanceof Array){
            if(result2.indexOf(true) !== -1){
                price =this.value1.value1.values[result1.indexOf(true)];
                result = true
            }
        }
        else{
            if(result2){
                result = true;
            }
        }
        return {result : result, price : price};
    }
    cross(value1 : any, value2 : any, type: string){
        let v1 = this.value1.blockFunction();
        let v2 = this.value2.blockFunction();
        switch (type) {
            case 'Down':
                var crossDown = new CrossDown({lineA: [], lineB: []});
                var results = [];
                v1.forEach((value, index) => {
                    var result = crossDown.nextValue(v1[index], v2[index]);
                    results.push(result)
                });   
                this.isReady = true;
                return results;
                break;
                
            case 'Up':
                var crossUp = new CrossUp({lineA: [], lineB: []});
                var results = [];
                v1.forEach((value, index) => {
                    var result = crossUp.nextValue(v1[index], v2[index]);
                    results.push(result)
                });    
                this.isReady = true;
                return results;
                break; 
            default:
                break;
        }
    }
    calculateStdDev(values : number[]){
        let mean = this.mean(values);
        let sigma = 0;
        for(let value of values){
            sigma += Math.pow((value - mean),2);
        }
        return Math.sqrt(sigma/values.length);
    }
    mean(values : number[]){
        let average=0;
        for(let value of this.values){
            average += value;
        }
        return (average/this.values.length)   
    }
    variance(values: number[]){
        let mean = this.mean(values);
        let sigma =  0;
        for(let value of values){
            sigma += Math.pow((value-mean),2);
        }
        return sigma / values.length-1;
    }
    middle(values : number[]){
        let length = this.values.length
        if(length % 2 === 0){
            return (this.values[(length / 2) - 1] + this.values[((length /2 + 1)) - 1])/2;
        }
        else {
            return this.values[((length+1) / 2) - 1];
        }
    }
}