import {DiagramLink} from './DiagramLinks';
import {DiagramNode} from './Diagramnode';
import {User} from './user';

export class Strategy {
    _id ?: string;
    Name : string;
    Nodes : DiagramNode [];
    Links : DiagramLink [];
    price? : number;
    forSell? : boolean;
    maker? : User;
}