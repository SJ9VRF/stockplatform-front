export interface DiagramLink {
    source : string;
    target : string;
}
