export interface Traders{
    currencyName: string,
    orders : [{
        value : number,
        unitPrice : number,
        totalPrice : number
    }]
    
}