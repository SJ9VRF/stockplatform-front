export interface TreeNode {
    name: string;
    id : string;
    children?: TreeNode[];
};