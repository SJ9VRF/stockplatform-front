import{User} from './user';

export interface News {
    _id : string,
    title : string,
    image : string,
    content : string,
    comments : [{author : User, content: string}]
}