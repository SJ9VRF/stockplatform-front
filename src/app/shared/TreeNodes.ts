import {TreeNode} from './TreeNode';

export const TREE_DATA : TreeNode[] = [
    {
        name: 'Indicator',
        id : 'I',
        children: [
          {name: 'MA', id : 'I-MA'},//SMA-WMA-EMA done
          {name: 'MACD', id : 'I-MACD'},//MACD done
          {name: 'Cross', id : 'I-Cross'},//cross done
          {name: 'RSI', id : 'I-RSI'},//RSI done
          {name: 'OBV', id : 'I-OBV'},//OBV done
          {name: 'ADX', id : 'I-ADX'},//ADX done
          {name: 'ATR', id : 'I-ATR'},//ATR done
          {name: 'Ichimoku', id : 'I-Ichimoku'},//Ichimoku done
          {name: 'BollingerBands', id : 'I-BollingerBands'},//done
          {name: 'MFI', id : 'I-MFI'},//done
          {name: 'CCI', id : 'I-CCI'},//done
          {name: 'Williams %R', id : 'I-Williams %R'},//done 
          {name: 'StochRsi', id : 'I-StochRsi'},//done
          {name: 'commonMath', id : 'I-commonMath'},//done
          {name: 'Stochastic', id : 'I-Stochastic'},//done
          {name: 'ParobolicSAR', id : 'I-ParobolicSAR'},//doesnt have this indicator
          
          
        ]
      }, {
        name: 'Logic',
        id: 'L',
        children: [
          {
            name: 'Comparison', id : 'L-Comparison'
          }, {
            name: 'And', id: 'L-And'
          },
          {
            name : 'Or', id : 'L-Or'
          }
        ]
    },
    {
      name : 'marketData',
      id : 'M',
      children : [
        {name : 'PriceData' , id : 'M-priceData'},
        {name : 'Symbol', id : 'M-symbol'}
      ]
    },
    {
      name : 'Trading',
      id : 'T',
      children : [
        {name : 'Signal' , id : 'T-Signal'}
      ]
    }
];
