export interface OutPut{
    name : string,
    profit : number[],
    startDate : number,
    endDate :  number,
    volume : number
  }