import {QuestionBase} from './question-base';
import{DropdownQuestion} from './question-dropdown';
import {TextBoxQuestion} from './question-textbox';


export let MAquestions : QuestionBase<string | number> [] = [

    new DropdownQuestion({
      key : 'timeframe',
      label : 'timeframe',
      selected : 'Current',
      options : [
        {key : 'Current', value : 'Current'},
        {key : 'Daily', value : 'Daily'},
        {key : 'Monthly', value : 'Monthly'},
        {key : 'Weekly', value : 'Weekly'}
      ],
      order : 1
    }),
    new DropdownQuestion({
      key : 'averageMethod',
      label : 'averageMethod',
      selected : 'Simple',
      options : [
        {key : 'Simple', value : 'Simple'},
        {key : 'Exponential', value : 'Exponential'},
        {key : 'LinearWeighted', value : 'LinearWeighted'},
        {key : 'Smoothed', value : 'Smoothed'}
      ],
      order : 2
    }),
    new DropdownQuestion({
      key : 'practicalPrice',
      label : 'practicalPrice',
      selected : 'Close',
      options : [
        {key : 'Close', value : 'Close'},
        {key : 'Open', value : 'Open'},
        {key : 'High', value : 'High'},
        {key : 'Low', value : 'Low'}
      ],
      order : 3
    }),
    new TextBoxQuestion({
      key : 'pricePullBack',
      label : 'pricePullBack',
      type : 'number',
      selected : '0',
      order : 5
    }),
    new TextBoxQuestion({
      key : 'period',
      label : 'period',
      type : 'number',
      selected : '14',
      order : 4
    })
  ];

  export let MACDquestions : QuestionBase<string | number> [] = [
    new DropdownQuestion({
        key : 'timeframe',
        label : 'timeframe',
        selected : 'Current',
        options : [
          {key : 'Current', value : 'Current'},
          {key : 'Daily', value : 'Daily'},
          {key : 'Monthly', value : 'Monthly'},
          {key : 'Weekly', value : 'Weekly'}
        ],
        order : 1
      }),
    new DropdownQuestion({
      key : 'practicalPrice',
      label : 'practicalPrice',
      selected : 'Close',
      options : [
        {key : 'Close', value : 'Close'},
        {key : 'Open', value : 'Open'},
        {key : 'High', value : 'High'},
        {key : 'Low', value : 'Low'}
      ],
      order : 5
    }),  
    new TextBoxQuestion({
        key : 'pricePullBack',
        label : 'pricePullBack',
        type : 'number',
        selected : '0',
        order : 6
      }) ,
    new TextBoxQuestion({
        key : 'fastPeriod',
        label : 'fastPeriod',
        type : 'number',
        selected : '12',
        order : 2
    }) ,
    new TextBoxQuestion({
        key : 'slowPeriod',
        label : 'slowPeriod',
        type : 'number',
        selected : '26',
        order : 3
    }) ,           
    new TextBoxQuestion({
        key : 'signalPeriod',
        label : 'signalPeriod',
        type : 'number',
        selected : '9',
        order : 4
    }) ,
    new DropdownQuestion({
        key : 'OutPut',
        label : 'OutPut',
        selected : 'Macd',
        options : [
        {key : 'Macd', value : 'Macd'},
        {key : 'Signal', value : 'Signal'},
        {key : 'Histogram', value : 'Histogram'}
        ],
        order : 7
    }),  
  ];

  export let Comparisonquestions : QuestionBase<string | number> [] = [
    new TextBoxQuestion({
      key : 'comparisonValue1',
      label : 'value1',
      type : 'string',
      selected : '',
      order : 1
  }) ,
    new TextBoxQuestion({
      key : 'comparisonValue2',
      label : 'value2',
      type : 'string',
      selected : '',
      order : 2
 }) ,
  new DropdownQuestion({
    key : 'type',
    label : 'type',
    selected : '=',
    options : [
    {key : '>', value : '>'},
    {key : '<', value : '<'},
    {key : '<=', value : '<='},
    {key : '>=', value : '>='},
    {key : '=', value : '='}
    ],
    order : 3
  }) 
  ];

  export let Crossquestions : QuestionBase <string | number>[] = [
    new TextBoxQuestion({
      key : 'value1',
      label : 'value1',
      type : 'string',
      selected : '',
      order : 1
  }) ,
    new TextBoxQuestion({
      key : 'value2',
      label : 'value2',
      type : 'string',
      selected : '',
      order : 2
 }) ,
  new DropdownQuestion({
    key : 'type',
    label : 'type',
    selected : '=',
    options : [
    {key : 'Up', value : 'Up'},
    {key : 'Down', value : 'Down'},
    ],
    order : 3
  })  
  ];

  export let PriceDataQuestion : QuestionBase <string | any> [] = [
    new DropdownQuestion({
      key : 'practicalPrice',
      label : 'practicalPrice',
      selected : 'Close',
      options : [
        {key : 'Close', value : 'Close'},
        {key : 'Open', value : 'Open'},
        {key : 'High', value : 'High'},
        {key : 'Low', value : 'Low'}
      ],
      order : 2
    }),
    new DropdownQuestion({
      key : 'timeframe',
      label : 'timeframe',
      selected : 'Current',
      options : [
        {key : 'Current', value : 'Current'},
        {key : 'Daily', value : 'Daily'},
        {key : 'Monthly', value : 'Monthly'},
        {key : 'Weekly', value : 'Weekly'}
      ],
      order : 1
    }),
  ]

  export let Signalquestions : QuestionBase<string | number> [] = [
  new DropdownQuestion({
    key : 'type',
    label : 'type',
    selected : '=',
    options : [
    {key : 'sell', value : 'sell'},
    {key : 'shop', value : 'shop'},
    ],
    order : 3
  }) 
  ];
  export let RSIquestions : QuestionBase<string | number> [] = [
    new DropdownQuestion({
      key : 'timeframe',
      label : 'timeframe',
      selected : 'Current',
      options : [
        {key : 'Current', value : 'Current'},
        {key : 'Daily', value : 'Daily'},
        {key : 'Monthly', value : 'Monthly'},
        {key : 'Weekly', value : 'Weekly'}
      ],
      order : 1
    }),
    new DropdownQuestion({
      key : 'practicalPrice',
      label : 'practicalPrice',
      selected : 'Close',
      options : [
        {key : 'Close', value : 'Close'},
        {key : 'Open', value : 'Open'},
        {key : 'High', value : 'High'},
        {key : 'Low', value : 'Low'}
      ],
      order : 3
    }),
    new TextBoxQuestion({
      key : 'pricePullBack',
      label : 'pricePullBack',
      type : 'number',
      selected : '0',
      order : 5
    }),
    new TextBoxQuestion({
      key : 'period',
      label : 'period',
      type : 'number',
      selected : '14',
      order : 4
    })
    ];
    export let ADXquestions : QuestionBase<string | number> [] = [
      new DropdownQuestion({
        key : 'timeframe',
        label : 'timeframe',
        selected : 'Current',
        options : [
          {key : 'Current', value : 'Current'},
          {key : 'Daily', value : 'Daily'},
          {key : 'Monthly', value : 'Monthly'},
          {key : 'Weekly', value : 'Weekly'}
        ],
        order : 1
      }),
      new TextBoxQuestion({
        key : 'pricePullBack',
        label : 'pricePullBack',
        type : 'number',
        selected : '0',
        order : 3
      }),
      new TextBoxQuestion({
        key : 'period',
        label : 'period',
        type : 'number',
        selected : '14',
        order : 2
      }),
      new DropdownQuestion({
        key : 'OutPut',
        label : 'OutPut',
        selected : 'Adx',
        options : [
        {key : 'Adx', value : 'Adx'},
        {key : '+DI', value : '+DI'},
        {key : '-DI', value : '-DI'}
        ],
        order : 4
    })
      ];  

      export let WilliamsRquestions : QuestionBase<string | number> [] = [
        new DropdownQuestion({
          key : 'timeframe',
          label : 'timeframe',
          selected : 'Current',
          options : [
            {key : 'Current', value : 'Current'},
            {key : 'Daily', value : 'Daily'},
            {key : 'Monthly', value : 'Monthly'},
            {key : 'Weekly', value : 'Weekly'}
          ],
          order : 1
        }),
        new TextBoxQuestion({
          key : 'pricePullBack',
          label : 'pricePullBack',
          type : 'number',
          selected : '0',
          order : 3
        }),
        new TextBoxQuestion({
          key : 'period',
          label : 'period',
          type : 'number',
          selected : '14',
          order : 2
        })
        ];  
    export let ATRquestions : QuestionBase<string | number> [] = [
        new DropdownQuestion({
          key : 'timeframe',
          label : 'timeframe',
          selected : 'Current',
          options : [
            {key : 'Current', value : 'Current'},
            {key : 'Daily', value : 'Daily'},
            {key : 'Monthly', value : 'Monthly'},
            {key : 'Weekly', value : 'Weekly'}
          ],
          order : 1
        }),
        new TextBoxQuestion({
          key : 'pricePullBack',
          label : 'pricePullBack',
          type : 'number',
          selected : '0',
          order : 3
        }),
        new TextBoxQuestion({
          key : 'period',
          label : 'period',
          type : 'number',
          selected : '14',
          order : 2
        })
        ];  
        export let MFIquestions : QuestionBase<string | number> [] = [
          new DropdownQuestion({
            key : 'timeframe',
            label : 'timeframe',
            selected : 'Current',
            options : [
              {key : 'Current', value : 'Current'},
              {key : 'Daily', value : 'Daily'},
              {key : 'Monthly', value : 'Monthly'},
              {key : 'Weekly', value : 'Weekly'}
            ],
            order : 1
          }),
          new TextBoxQuestion({
            key : 'pricePullBack',
            label : 'pricePullBack',
            type : 'number',
            selected : '0',
            order : 3
          }),
          new TextBoxQuestion({
            key : 'period',
            label : 'period',
            type : 'number',
            selected : '14',
            order : 2
          })
          ];
          
        export let CCIquestions : QuestionBase<string | number> [] = [
            new DropdownQuestion({
              key : 'timeframe',
              label : 'timeframe',
              selected : 'Current',
              options : [
                {key : 'Current', value : 'Current'},
                {key : 'Daily', value : 'Daily'},
                {key : 'Monthly', value : 'Monthly'},
                {key : 'Weekly', value : 'Weekly'}
              ],
              order : 1
            }),
            new TextBoxQuestion({
              key : 'pricePullBack',
              label : 'pricePullBack',
              type : 'number',
              selected : '0',
              order : 3
            }),
            new TextBoxQuestion({
              key : 'period',
              label : 'period',
              type : 'number',
              selected : '14',
              order : 2
            })
            ];  
          export let OBVquestions : QuestionBase<string | number> [] = [
              new DropdownQuestion({
                key : 'timeframe',
                label : 'timeframe',
                selected : 'Current',
                options : [
                  {key : 'Current', value : 'Current'},
                  {key : 'Daily', value : 'Daily'},
                  {key : 'Monthly', value : 'Monthly'},
                  {key : 'Weekly', value : 'Weekly'}
                ],
                order : 1
              }),
              new TextBoxQuestion({
                key : 'pricePullBack',
                label : 'pricePullBack',
                type : 'number',
                selected : '0',
                order : 4
              }),
              new TextBoxQuestion({
                key : 'period',
                label : 'period',
                type : 'number',
                selected : '14',
                order : 2
              })
              ];
              export let Bollingerquestions : QuestionBase<string | number> [] = [
                new DropdownQuestion({
                    key : 'timeframe',
                    label : 'timeframe',
                    selected : 'Current',
                    options : [
                      {key : 'Current', value : 'Current'},
                      {key : 'Daily', value : 'Daily'},
                      {key : 'Monthly', value : 'Monthly'},
                      {key : 'Weekly', value : 'Weekly'}
                    ],
                    order : 1
                  }),
                new DropdownQuestion({
                  key : 'practicalPrice',
                  label : 'practicalPrice',
                  selected : 'Close',
                  options : [
                    {key : 'Close', value : 'Close'},
                    {key : 'Open', value : 'Open'},
                    {key : 'High', value : 'High'},
                    {key : 'Low', value : 'Low'}
                  ],
                  order : 5
                }),  
                new TextBoxQuestion({
                    key : 'pricePullBack',
                    label : 'pricePullBack',
                    type : 'number',
                    selected : '0',
                    order : 6
                  }) ,
                new TextBoxQuestion({
                    key : 'period',
                    label : 'period',
                    type : 'number',
                    selected : '14',
                    order : 2
                }) ,         
                new TextBoxQuestion({
                    key : 'stdDev',
                    label : 'stdDev',
                    type : 'number',
                    selected : '2',
                    order : 4
                }) ,
                new DropdownQuestion({
                    key : 'OutPut',
                    label : 'OutPut',
                    selected : 'Macd',
                    options : [
                    {key : 'Lower', value : 'Lower'},
                    {key : 'Middle', value : 'Middle'},
                    {key : 'Upper', value : 'Upper'}
                    ],
                    order : 6
                }),  
              ];
            export let stochRSIquestions : QuestionBase<string | number> [] = [
              new DropdownQuestion({
                key : 'timeframe',
                label : 'timeframe',
                selected : 'Current',
                options : [
                  {key : 'Current', value : 'Current'},
                  {key : 'Daily', value : 'Daily'},
                  {key : 'Monthly', value : 'Monthly'},
                  {key : 'Weekly', value : 'Weekly'}
                ],
                order : 1
              }),
              new TextBoxQuestion({
                key : 'kPeriod',
                label : 'kPeriod',
                type : 'number',
                selected : '14',
                order : 2
              }) ,
            new TextBoxQuestion({
              key : 'dPeriod',
              label : 'dPeriod',
              type : 'number',
              selected : '1',
              order : 3
            }) ,
            new TextBoxQuestion({
              key : 'rsiPeriod',
              label : 'rsiPeriod',
              type : 'number',
              selected : '3',
              order : 4
          }),
          new TextBoxQuestion({
            key : 'stochasticPeriod',
            label : 'stochasticPeriod',
            type : 'number',
            selected : '5',
            order : 5
        }) ,
        new TextBoxQuestion({
          key : 'pricePullBack',
          label : 'pricePullBack',
          type : 'number',
          selected : '0',
          order : 6
        }),
        ] ;
        export let Ichimokuquestions : QuestionBase<string | number> [] = [
          new DropdownQuestion({
            key : 'timeframe',
            label : 'timeframe',
            selected : 'Current',
            options : [
              {key : 'Current', value : 'Current'},
              {key : 'Daily', value : 'Daily'},
              {key : 'Monthly', value : 'Monthly'},
              {key : 'Weekly', value : 'Weekly'}
            ],
            order : 1
          }),
          new TextBoxQuestion({
            key : 'conversionPeriod',
            label : 'conversionPeriod',
            type : 'number',
            selected : '9',
            order : 2
          }),
          new TextBoxQuestion({
            key : 'basePeriod',
            label : 'basePeriod',
            type : 'number',
            selected : '26',
            order : 3
          }),
          new TextBoxQuestion({
            key : 'spanPeriod',
            label : 'spanPeriod',
            type : 'number',
            selected : '52',
            order : 4
          }),
          new TextBoxQuestion({
            key : 'displacement',
            label : 'displacement',
            type : 'number',
            selected : '26',
            order : 5
          })
        ];              
        export let commonMathquestions : QuestionBase<string | number> [] = [
          new DropdownQuestion({
            key : 'timeframe',
            label : 'timeframe',
            selected : 'Current',
            options : [
              {key : 'Current', value : 'Current'},
              {key : 'Daily', value : 'Daily'},
              {key : 'Monthly', value : 'Monthly'},
              {key : 'Weekly', value : 'Weekly'}
            ],
            order : 1
          }),
          new TextBoxQuestion({
            key : 'period',
            label : 'period',
            type : 'number',
            selected : '14',
            order : 2
          }),
          new DropdownQuestion({
            key : 'mathMethod',
            label : 'mathMethod',
            selected : 'min',
            options : [
              {key : 'min', value : 'min'},
              {key : 'max', value : 'max'},
              {key : 'sum', value : 'sum'},
              {key : 'middle', value : 'middle'},
              {key : 'average', value : 'average'},
              {key : 'exponentialAverage', value : 'exponentialAverage'},
              {key : 'standardDeviation', value : 'standardDeviation'},
              {key : 'variance', value : 'variance'},
              {key : 'count', value : 'count'},
            ],
            order : 4
          }),
          new TextBoxQuestion({
            key : 'pricePullBack',
            label : 'pricePullBack',
            type : 'number',
            selected : '0',
            order : 5
          }),
          new DropdownQuestion({
            key : 'practicalPrice',
            label : 'practicalPrice',
            selected : 'Close',
            options : [
              {key : 'Close', value : 'Close'},
              {key : 'Open', value : 'Open'},
              {key : 'High', value : 'High'},
              {key : 'Low', value : 'Low'}
            ],
            order :6
          }),
        ]
        export let Stochasticquestions : QuestionBase<string | number> [] = [  
          new DropdownQuestion({
            key : 'timeframe',
            label : 'timeframe',
            selected : 'Current',
            options : [
              {key : 'Current', value : 'Current'},
              {key : 'Daily', value : 'Daily'},
              {key : 'Monthly', value : 'Monthly'},
              {key : 'Weekly', value : 'Weekly'}
            ],
            order : 1
          }),
          new TextBoxQuestion({
            key : 'period',
            label : 'period',
            type : 'number',
            selected : '14',
            order : 2
          }),
          new TextBoxQuestion({
            key : 'signalPeriod',
            label : 'signalPeriod',
            type : 'number',
            selected : '3',
            order : 3
          })
        ]