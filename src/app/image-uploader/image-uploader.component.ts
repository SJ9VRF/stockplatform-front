//upload profile image
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import{ FileUploader } from 'ng2-file-upload';
import {baseurl} from '../shared/baseurl';
import {AuthService} from '../services/auth.service';
import{Ng2ImgMaxService} from 'ng2-img-max';
@Component({
  selector: 'app-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.scss']
})
export class ImageUploaderComponent implements OnInit {

 // imagename : string = '20180712_193415.jpg';
  
  @Input() imageName : string ;
  @Output() imageNameChange = new EventEmitter<string>();

  imageAddress : string;

  public uploader : FileUploader = new FileUploader({
    url : baseurl + 'upload/profileimage',
    itemAlias : 'profileImage',
    authToken : "Bearer " + this.authService.getToken()
  });

  constructor(private authService : AuthService, private _ng2ImgMax : Ng2ImgMaxService) { 

  }

  ngOnInit() {

    this.imageAddress= baseurl + 'images/profile/' + this.imageName;
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials =false;
    };
    this.uploader.onCompleteItem = (item : any, status : any) => {
      console.log('Uploaded File Details: ', item);
      this.imageName = item.file.name;
      this.imageNameChange.emit(this.imageName);
      this.imageAddress = baseurl + 'images/profile/' + this.imageName;
    };

 //   this.uploader.authTokenHeader = `Authorization: bearer ${this.authService.getToken()}`;
  }
  handleUpload(event ) {
    this._ng2ImgMax.resizeImage(event.target.files[0], 100, 100).subscribe(
      result => {
        const newImage = new File([result], result.name);
        this.uploader.clearQueue();
        this.uploader.addToQueue([event.target.files[0]]);
        this.uploader.uploadAll();
      },
      error => console.log(error)
    );
  }

}
