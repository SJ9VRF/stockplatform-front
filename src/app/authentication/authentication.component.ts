import { Component, OnInit, ɵConsole } from '@angular/core';
import{ FileUploader } from 'ng2-file-upload';
import{baseurl} from '../shared/baseurl';
import{AuthService} from '../services/auth.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import {TranslocoService} from '@ngneat/transloco';
import { ProfileManagmentService } from '../services/profile-managment.service';
import { Console } from 'console';
import { User } from '../shared/user';
import{ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {

  personalInfo : FormGroup;
  bankInfo : FormGroup;
  imageAddress : string;
  imageName : string;
  userId : string;
  URL : string ;
  user : User;
  admin : boolean = false;
  description : string;
  defualtImage : string;
  //date picker direction
  config = {
    drops : 'down'
  }
  constructor(private authService : AuthService,private fb : FormBuilder,private translocoService : TranslocoService,
    private profileManagmentService : ProfileManagmentService, private route : ActivatedRoute) { }
  
  public uploader : FileUploader = new FileUploader({
    url : baseurl + 'upload/authentication  ',
    itemAlias : 'authImage',
    authToken : "Bearer " + this.authService.getToken()
  });


  ngOnInit(): void {
    this.authService.getAdmin().subscribe((admin) => {this.admin = admin; console.log('adminnnn', this.admin)});
    this.authService.loadUserCredentials();
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials =false;
    };
    this.uploader.onCompleteItem = (item : any, status : any) => {
      console.log('Uploaded File Details: ', item);
      this.imageName = item.file.name;
      this.imageAddress = this.URL + this.imageName;
      this.profileManagmentService.updateUserProfile({documnetPath : this.imageAddress}).subscribe((result) => {
        console.log(result);
        })
    };
    if(this.admin){
      let userId = this.route.snapshot.params.userId;    
      //after admin click on  the specific user in indentityConfirmation table the link navigate to this component with user id
      //and this function get that specific user information
      this.profileManagmentService.getUserByAdmin(userId).subscribe((user) => {
        console.log('user', user)
        this.userId = user._id;
        this.URL =  baseurl + 'images/auth/'+ this.userId+ '/';
        this.defualtImage = baseurl + 'images/'+ 'summer.jpg';
        this.initialValues.firstName = user.firstName? user.firstName : '';
        this.initialValues.lastName = user.lastName? user.lastName : '';
        this.initialValues.nationalCode = user.nationalCode? user.nationalCode : '';
        this.initialValues.telNumber = user.telNumber? user.telNumber : '';
        this.initialValues.birthDay = user.birthDay? user.birthDay : '';
        this.initialValues.phone = user.phone? user.phone : '';
        this.initialValues.address = user.address? user.address : '';
        this.initialValues.cardNumber = user.cardNumber? user.cardNumber : '';
        this.initialValues.shabaCode = user.shabaCode? user.shabaCode : '';
        this.imageAddress = user.documnetPath?  user.documnetPath :  this.defualtImage ;
        this.creatForm();
      });
    }
    else {
      //when user come to this page he should see own information and compelete them.
      this.profileManagmentService.getUserProfile().subscribe((user) => {
        this.userId = user._id;
        this.URL =  baseurl + 'images/auth/'+ this.userId+ '/';
        this.defualtImage = baseurl + 'images/'+ 'summer.jpg';
        this.initialValues.firstName = user.firstName? user.firstName : '';
        this.initialValues.lastName = user.lastName? user.lastName : '';
        this.initialValues.nationalCode = user.nationalCode? user.nationalCode : '';
        this.initialValues.telNumber = user.telNumber? user.telNumber : '';
        this.initialValues.birthDay = user.birthDay? user.birthDay : '';
        this.initialValues.phone = user.phone? user.phone : '';
        this.initialValues.address = user.address? user.address : '';
        this.initialValues.cardNumber = user.cardNumber? user.cardNumber : '';
        this.initialValues.shabaCode = user.shabaCode? user.shabaCode : '';
        this.imageAddress = user.documnetPath?  user.documnetPath :  this.defualtImage ;
        this.creatForm();
    });
    }
  }

  

  initialValues ={
    firstName : '',
    lastName : '',
    nationalCode : '',
    telNumber : '',
    birthDay : '',
    phone : '',
    address: '',
    cardNumber : '',
    shabaCode : ''
  }
  formErrors = {
     'firstName' : '',
      'lastName' : '',
      'username' : '',
      'nationalCode' : '',
      'telNumber' : '',
      'birthDay' : '',
      'phone' : '',
      'address': '',
      'cardNumber' : '',
      'shabaCode' : ''
    };
    validationMessages = {
      'firstName' : {
        'required': 'نام الزامی است'
        },
      'lastName' : { 
        'required': 'نام خانوادگی الزامی است'
      },
      'username' : { 
        'required': 'نام کاربری الزامی است'
      },
      'nationalCode' : { 
        'required': 'کد ملی الزامی است'
      },
      'telNumber' : { 
        'required': 'شماره ی موبایل الزامی است',
        'pattern' : 'فرمت شماره تلفن نادرست است',
        'minlength' : 'شماره موبایل 11 رقمی وارد کنید',
        'maxlength' : 'شماره موبایل 11 رقمی وارد کنید'
      },
      'shabaCode' : { 
        'required': 'آدرس ایمیل الزامی است',
      },
      'cardNumber' : {
        'required' : 'رمز عبور الزامی است'
      },
      'phone' :{
        'required' : 'تلفن ثابت الزامی است'
      },
      'birthDay' : {
        'required' : ''
      },
      'address' : {
        'required' : ''
      }
    }

    creatForm(){
      this.personalInfo = this.fb.group({
        firstName : [this.initialValues.firstName,[ Validators.required]],
        lastName : [this.initialValues.lastName,[ Validators.required]],
        nationalCode : [this.initialValues.nationalCode, [Validators.required]],
        telNumber : [this.initialValues.telNumber, [Validators.pattern, Validators.maxLength(11),
         Validators.minLength(11)]],
        phone : [this.initialValues.phone,[ Validators.required ]],
        birthDay : [this.initialValues.birthDay , [Validators.required]],
        address : [this.initialValues.address , [Validators.required]]
      });
      this.bankInfo = this.fb.group({
        cardNumber : [this.initialValues.cardNumber , [Validators.required]],
        shabaCode : [this.initialValues.shabaCode , [Validators.required]]
      })
      this.bankInfo.valueChanges.subscribe((data) => this.onValueChangedBank(data));
      this.personalInfo.valueChanges.subscribe((data) => this.onValueChanged(data));
    }
    //when the form field get dirty this fuction check the field for errors
    onValueChanged(data : any){
      if(!this.personalInfo){return ;}
      const form = this.personalInfo;
      //clear previous err message
      for(const field in this.formErrors){
        if(this.formErrors.hasOwnProperty(field)){
          this.formErrors[field] = '';
          const control = form.get(field);
          //re check for erros
          if(control && control.dirty && !control.valid){
            const messages = this.validationMessages[field];
            console.log('control errors : ' ,control.errors);
            for(const key in control.errors){
              if(control.errors.hasOwnProperty(key)){
                console.log('key =' , key);
                console.log('message[key]', messages);
                this.formErrors[field] += this.translocoService.translate(`formErrors.${field}.${key}`) + '\t';
              }
            }
          }
        }
      }
    }
    //when the form field get dirty this fuction check the field for errors
    onValueChangedBank(data : any){
      if(!this.bankInfo){return ;}
      const form = this.bankInfo;
      //clear previous err message
      for(const field in this.formErrors){
        if(this.formErrors.hasOwnProperty(field)){
          this.formErrors[field] = '';
          const control = form.get(field);
          //re check for erros
          if(control && control.dirty && !control.valid){
            const messages = this.validationMessages[field];
            console.log('control errors : ' ,control.errors);
            for(const key in control.errors){
              if(control.errors.hasOwnProperty(key)){
                console.log('key =' , key);
                console.log('message[key]', messages);
                this.formErrors[field] += this.translocoService.translate(`formErrors.${field}.${key}`) + '\t';
              }
            }
          }
        }
      }
    }

  //this function call when user submit his information
  submit(){
    this.user = this.personalInfo.value;
    this.user.cardNumber = this.bankInfo.get('cardNumber').value;
    this.user.shabaCode = this.bankInfo.get('shabaCode').value;
    this.user.status = this.translocoService.translate('inProgress');
    this.profileManagmentService.updateUserProfile(this.user).subscribe((result) => {
    console.log(result);
    })
  }
  //this function call when admin confirm the user informations and documents
  submitDoc(){
    this.profileManagmentService.updateUserProfile({status : "Confirmation",description : this.description}, this.userId).subscribe((result) => {
      console.log(result);
      })
  }
  //this function call when admin dont confirm user info...
  defectiveDoc(){
    console.log('description', this.description);
    this.profileManagmentService.updateUserProfile({status : "Defective documnets" ,description : this.description},this.userId).subscribe((result) => {
      console.log(result);
      })

  }
}
