import { Component, OnInit, Input, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { QuestionBase } from '../shared/question/question-base';
import { FormGroup } from '@angular/forms';
import{ QuestionControlService } from '../services/question-control.service';
import {Subject} from 'rxjs';
import{TranslocoService} from '@ngneat/transloco';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss'],
  providers : [ QuestionControlService ]
})
export class DynamicFormComponent implements OnInit {

  @Input() questions : QuestionBase<string | number > [];
  @Input() change: boolean;
  form : FormGroup;
  @Output() payLoad = new EventEmitter<any>();

  constructor(private qcs : QuestionControlService, private translocoService : TranslocoService) { }

  ngOnInit() {
    for(let question of this.questions){
      let lang = this.translocoService.getActiveLang();
      console.log('lang', lang)
      question.label = this.translocoService.translate(question.key,{}, lang);
    }
    this.form = this.qcs.toFormGroup(this.questions);
  }
  
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      let chng = changes[propName];
      let cur  = chng.currentValue;
      let prev = chng.previousValue;
      console.log('prev',prev);
      console.log('curr',cur);
      if(propName==="change"){
        for(let question of this.questions){
          let lang = this.translocoService.getActiveLang();
          console.log('lang', lang)
          question.label = this.translocoService.translate(question.key,{}, lang);
          if(question.options){
            for(let opt of question.options) {
              opt.value = this.translocoService.translate(opt.key,{}, lang);
            }
            console.log('options', question.options)
          }
        }
        console.log('changeQuestions',this.questions);
        this.form = this.qcs.toFormGroup(this.questions);
      }
    }
  }

  onSubmit(){
    this.payLoad.emit(this.form.getRawValue());
    console.log('payload',this.form.getRawValue() );
  }

}
