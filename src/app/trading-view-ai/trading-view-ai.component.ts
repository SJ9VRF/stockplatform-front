//simple trading view charts for digital currency page
import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Input, SimpleChanges, HostListener } from '@angular/core';
import { createChart } from 'lightweight-charts';
import{StrategyService} from '../services/strategy.service';
import{AuthService} from '../services/auth.service';
import { ApiService } from '../services/api.service';
import { id } from '@swimlane/ngx-charts';

declare const TradingView: any;

@Component({
  selector: 'app-trading-view-ai',
  templateUrl: './trading-view-ai.component.html',
  styleUrls: ['./trading-view-ai.component.scss']
})

export class TradingViewAiComponent implements OnInit {

  @Input() collectionName : string;
  @Input() selectedStock : string;
  @Input() startDay : number;
  @Input() endDay : number;
  hCoefficient: number = 0.4;
  wCoefficient: number = 0.4;
  view: number[]=[];
  chart : any;
  constructor(private strategyService : StrategyService, private authService : AuthService,
    private apiService: ApiService) { }
  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.view[1] = window.innerWidth * this.hCoefficient;
        this.view[0] = window.innerWidth * this.wCoefficient;
        this.chart.resize(this.view[0],this.view[1]);
        this.chart.applyOptions({
          layout: {
              fontSize: 0.01 *window.innerWidth ,
          },
      });        
  }
  ngOnInit() {
    this.view[0] = window.innerWidth * this.wCoefficient;
    this.view[1] = window.innerHeight * this.hCoefficient;

    let myChart = document.getElementById('chart');
    this.authService.loadUserCredentials();
    this.chart = createChart(myChart, { width: this.view[0], height: this.view[1] });
    this.chart.applyOptions({
      layout: {
          fontSize: 0.01 *window.innerWidth ,
      },
  });
    if(this.collectionName){
          //this.collectionName = "آرمان"
        
        console.log('tred colname', this.collectionName);

        const lineSeries = this.chart.addLineSeries();
        this.strategyService.getPriceData('Daily','Close',this.collectionName).subscribe((values) => {
        for(let value of values) {
          console.log('value', value);
          lineSeries.update({time : value.time , value : value.price})
        }
      })
    }if(this.selectedStock){
        const lineSeries = this.chart.addLineSeries();
        this.apiService.predictor(this.selectedStock ,this.startDay,this.endDay).subscribe((result) => {
          for(let value of result) {
            console.log('resultttttttttttttttttttttttttttttttttttt', value);
            lineSeries.update({time : result[0].prediction_value.time , value : result[1].prediction_value.price})
          }
      })
}
}
  
ngOnChanges(changes: SimpleChanges) {
  for (let propName in changes) {
    let chng = changes[propName];
    let cur  = chng.currentValue;
    let prev = chng.previousValue;
    console.log('prev',prev);
    console.log('curr',cur);
    if(propName === 'collectionName' && prev){
      console.log('changecolName',this.collectionName);
      let myChart = document.getElementById('chart');
      myChart.removeChild(myChart.firstChild);
      const chart = createChart(myChart, { width: 600, height: 500 });
      const lineSeries = chart.addLineSeries();
      this.strategyService.getPriceData('Daily','Close',this.collectionName).subscribe((values) => {
      for(let value of values) {
        lineSeries.update({time : value.time , value : value.price})
      }
    })
    }if(this.selectedStock){
      console.log('else part occured',this.selectedStock);
      let myChart = document.getElementById('chart');
      myChart.removeChild(myChart.firstChild);
      const chart = createChart(myChart, { width: 600, height: 500 });
      const lineSeries = chart.addLineSeries();
      this.apiService.predictor(this.selectedStock ,this.startDay,this.endDay).subscribe((results) => {
        for(let result of results[0]) {
          console.log('resultttttttttttttttttttttttttttttttttttt', result);
          lineSeries.update({time : result.prediction_value.time , value : result.prediction_value.price})
        }
    })
    }
  }
}

}
