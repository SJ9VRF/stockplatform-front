import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradingViewAiComponent } from './trading-view-ai.component';

describe('TradingViewAiComponent', () => {
  let component: TradingViewAiComponent;
  let fixture: ComponentFixture<TradingViewAiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradingViewAiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradingViewAiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
