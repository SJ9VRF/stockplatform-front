import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadStrategyComponent } from './load-strategy.component';

describe('LoadStrategyComponent', () => {
  let component: LoadStrategyComponent;
  let fixture: ComponentFixture<LoadStrategyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadStrategyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadStrategyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
