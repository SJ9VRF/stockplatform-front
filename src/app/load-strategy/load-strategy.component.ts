import { Component, OnInit, Inject } from '@angular/core';
import{MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogData } from '../shared/dialogData';

@Component({
  selector: 'app-load-strategy',
  templateUrl: './load-strategy.component.html',
  styleUrls: ['./load-strategy.component.scss']
})
export class LoadStrategyComponent implements OnInit {

  constructor(public dialogRef : MatDialogRef<LoadStrategyComponent> ,
    @Inject(MAT_DIALOG_DATA) public data : DialogData) { }

  ngOnInit() {
  }
  closeLoadDialog(){
    this.dialogRef.close();
  }
}
