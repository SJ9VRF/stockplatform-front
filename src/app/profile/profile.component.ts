import { Component, OnInit , OnDestroy} from '@angular/core';
import { FormGroup , FormBuilder, Validators} from '@angular/forms';
import{ProfileManagmentService} from '../services/profile-managment.service';
import{AuthService} from '../services/auth.service';
import { User } from '../shared/user';
import {Subscription} from 'rxjs';
import {TranslocoService} from '@ngneat/transloco';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {

  subscription : Subscription;
  visibility : boolean;
  profileForm : FormGroup;
  profile : User;
  admin : boolean;

  config = {
    drops : "up"
  }
  initialValues ={
    firstName : '',
    lastName : '',
    username : '',
    nationalCode : '',
    telNumber : '',
    email : '',
    image : '',
    birthDay : ''
  }
  formErrors = {
     'firstName' : '',
      'lastName' : '',
      'username' : '',
      'nationalCode' : '',
      'telNumber' : '',
      'email' : '',
      'password': '',
      'birthDay' : ''
    };
    validationMessages = {
      'firstName' : {
        'required': 'نام الزامی است'
        },
      'lastName' : { 
        'required': 'نام خانوادگی الزامی است'
      },
      'username' : { 
        'required': 'نام کاربری الزامی است'
      },
      'nationalCode' : { 
        'required': 'کد ملی الزامی است'
      },
      'telNumber' : { 
        'required': 'شماره ی موبایل الزامی است',
        'pattern' : 'فرمت شماره تلفن نادرست است',
        'minlength' : 'شماره موبایل 11 رقمی وارد کنید',
        'maxlength' : 'شماره موبایل 11 رقمی وارد کنید'
      },
      'email' : { 
        'required': 'آدرس ایمیل الزامی است',
        'email' : 'فرمت ایمیل نادرست است'
      },
      'password' : {
        'required' : 'رمز عبور الزامی است'
      },
      'birthDay' : {
        'required' : 'تاریخ تولد الزامی است'
      }
    }



  constructor(private fb : FormBuilder,
    private profileManagmentService : ProfileManagmentService,private translocoService : TranslocoService,
    private authService : AuthService) { }

  ngOnInit() {
    //chack this user is admin or not
    this.authService.getAdmin().subscribe((admin) => {this.admin = admin; console.log('admin', this.admin)});
    //load jwt
    this.authService.loadUserCredentials();
    this.subscription = this.authService.getVisibility()
    .subscribe((visibility) => {
      this.visibility = visibility;
    });
    //set initial values for form fields
    this.profileManagmentService.getUserProfile().subscribe((user) => {
        this.initialValues.firstName = user.firstName? user.firstName : '';
        this.initialValues.lastName = user.lastName? user.lastName : '';
        this.initialValues.username = user.username? user.username : '';
        this.initialValues.nationalCode = user.nationalCode? user.nationalCode : '';
        this.initialValues.telNumber = user.telNumber? user.telNumber : '';
        this.initialValues.email = user.email? user.email : '';
        this.initialValues.image = user.image? user.image : 'sea.jpg';
        console.log('birth', user.birthDay);
        this.initialValues.birthDay = user.birthDay? user.birthDay : '';
        this.creatForm();
        this.visibility = true;
    });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
  creatForm(){
    this.profileForm = this.fb.group({
      firstName : [this.initialValues.firstName,[ Validators.required]],
      lastName : [this.initialValues.lastName,[ Validators.required]],
      username : [this.initialValues.username, [Validators.required]],
      nationalCode : [this.initialValues.nationalCode, [Validators.required]],
      telNumber : [this.initialValues.telNumber, [Validators.pattern, Validators.maxLength(11),
         Validators.minLength(11)]],
      email : [this.initialValues.email,[ Validators.required , Validators.email]],
      password : ['' , [Validators.required]],
      birthDay : [this.initialValues.birthDay , [Validators.required]],
    });   
    this.profileForm.valueChanges.subscribe((data) => this.onValueChanged(data));
  }

  onValueChanged(data : any){
    if(!this.profileForm){return ;}
    const form = this.profileForm;
    //clear previous err message
    for(const field in this.formErrors){
      if(this.formErrors.hasOwnProperty(field)){
        this.formErrors[field] = '';
        const control = form.get(field);
        //re check for erros
        if(control && control.dirty && !control.valid){
          const messages = this.validationMessages[field];
          console.log('control errors : ' ,control.errors);
          for(const key in control.errors){
            if(control.errors.hasOwnProperty(key)){
              console.log('key =' , key);
              console.log('message[key]', messages);
              this.formErrors[field] += this.translocoService.translate(`formErrors.${field}.${key}`) + '\t';
            }
          }
        }
      }
    }
  }
  imageChange(imageName : string){
    this.profileManagmentService.changeProfileImage(imageName).subscribe((user) =>{
      console.log('user', user);
    })
  }
  updateUserProfile(){
    this.profile = this.profileForm.value;
    this.profileManagmentService.updateUserProfile(this.profile).subscribe((result) => {
      console.log(result);
    })
  }


}
