import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import{DragdropService} from './services/dragdrop.service';
import{StrategyService} from './services/strategy.service';
import { ProcessHttpErrorService } from './services/process-http-error.service';
import {AuthService} from './services/auth.service';
import{ProfileManagmentService} from './services/profile-managment.service';
import{AuthInterceptor} from './services/auth.interceptor';
import{ NewsService } from './services/news.service';
import {DigitalCurrencyService} from './services/digital-currency.service';
import{ApiService} from './services/api.service';

//var tulind = require('tulind');

import { HeaderComponent } from './header/header.component';
import { CollectionNamesComponent } from './collection-names/collection-names.component';
import { CreditComponent } from './credit/credit.component';
import { DigitalCurrencyComponent } from './digital-currency/digital-currency.component';
import { DynamicFormQuestionComponent } from './dynamic-form-question/dynamic-form-question.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { ImageUploaderComponent } from './image-uploader/image-uploader.component';
import { LoadStrategyComponent } from './load-strategy/load-strategy.component';
import { LoginComponent } from './login/login.component';
import { MakeNewsComponent } from './make-news/make-news.component';
import { NewsComponent } from './news/news.component';
import { NewsDetailComponent } from './news-detail/news-detail.component';
import { NewsImageUploaderComponent } from './news-image-uploader/news-image-uploader.component';
import { ProfileComponent } from './profile/profile.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SaveStrategyComponent } from './save-strategy/save-strategy.component';
import { SignupComponent } from './signup/signup.component';
import { StrategyComponent } from './strategy/strategy.component';
import { TradersTableComponent } from './traders-table/traders-table.component';
import { TradingViewComponent } from './trading-view/trading-view.component';
import { ShopComponent } from './shop/shop.component';
import { StrategyOutputComponent } from './strategy-output/strategy-output.component';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { TestPackageComponent } from './test-package/test-package.component';
import {AuthenticationComponent} from './authentication/authentication.component';
import{IdentityConfirmationComponent} from './identity-confirmation/identity-confirmation.component';


import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import{BrowserAnimationsModule} from '@angular/platform-browser/animations';
import{FlexLayoutModule} from '@angular/flex-layout';
import{FormsModule , ReactiveFormsModule} from '@angular/forms';
import{AppRoutingModule} from './app-routing/app-routing.module';
import{ FileUploadModule } from 'ng2-file-upload';
import{Ng2ImgMaxModule} from 'ng2-img-max';
import{AvatarModule} from 'ngx-avatar';
import{ AngularEditorModule } from '@kolkov/angular-editor';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import{MatFormFieldModule} from '@angular/material/form-field'; 
import{MatToolbarModule} from '@angular/material/toolbar';
import{MatDialogModule } from '@angular/material/dialog';
import{MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import{MatInputModule} from '@angular/material/input';
import{MatSelectModule} from '@angular/material/select';
import{MatLabel} from '@angular/material/form-field';
import { MatDividerModule} from '@angular/material/divider';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';
import{MatTableModule} from '@angular/material/table';
import{MatSortModule} from '@angular/material/sort';
import{ MatPaginatorModule} from '@angular/material/paginator';
import {MatDatepickerModule} from '@angular/material/datepicker';  
import{CdkTreeModule} from '@angular/cdk/tree';
import{MatTreeModule} from '@angular/material/tree';
import{ MatSlideToggleModule} from '@angular/material/slide-toggle';
import{StarRatingModule} from 'angular-star-rating';
import {NgxDiagramModule} from 'ngx-diagram';
import{MatGridListModule} from '@angular/material/grid-list';
import {MatMenuModule} from '@angular/material/menu';
import {DndModule} from 'ngx-drag-drop';
import {DpDatePickerModule} from 'ng2-jalali-date-picker';
import { RunStrategyComponent } from './run-strategy/run-strategy.component';
import { TranslocoRootModule } from './transloco-root.module';
import{ConvertNumberPipe} from './pipes/convert-number.pipe';
import {MatTabsModule} from '@angular/material/tabs';
import{PortfolioComponent} from './portfolio/portfolio.component';
import{AiEngineComponent} from './ai-engine/ai-engine.component';
import { TradingViewAiComponent } from './trading-view-ai/trading-view-ai.component';
import { TechnicalComponent } from './technical/technical.component';
import { TourComponent } from './tour/tour.component';


@NgModule({
  declarations: [
    PortfolioComponent,
    AiEngineComponent,
    AppComponent,
    HeaderComponent,
    CollectionNamesComponent,
    CreditComponent,
    DigitalCurrencyComponent,
    DynamicFormQuestionComponent,
    FooterComponent,
    HomeComponent,
    ImageUploaderComponent,
    LoadStrategyComponent,
    LoginComponent,
    MakeNewsComponent,
    NewsComponent,
    NewsDetailComponent,
    NewsImageUploaderComponent,
    ProfileComponent,
    ResetPasswordComponent,
    SaveStrategyComponent,
    SignupComponent,
    StrategyComponent,
    TradersTableComponent,
    TradingViewComponent,
    ShopComponent,
    StrategyOutputComponent,
    DynamicFormComponent,
    TestPackageComponent,
    RunStrategyComponent,
    ConvertNumberPipe,
    AuthenticationComponent,
    IdentityConfirmationComponent,
    TradingViewAiComponent,
    TechnicalComponent,
    TourComponent

  ],
  imports: [
    BrowserModule,
    NgxDiagramModule,
    FileUploadModule,
    DpDatePickerModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    DndModule,
    CdkTreeModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatTreeModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    MatCardModule,
    MatListModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatGridListModule,
    MatTabsModule,
    MatMenuModule,
    Ng2ImgMaxModule,
    AvatarModule,
    AngularEditorModule,
    TranslocoRootModule,
    NgxChartsModule 

   // StarRatingModule.forRoot()
  ],
  providers: [
    DragdropService,
    StrategyService,
    ProcessHttpErrorService,
    AuthService,
    ProfileManagmentService,
    NewsService,
    DigitalCurrencyService,
    ApiService,
    {
      provide : HTTP_INTERCEPTORS,
      useClass : AuthInterceptor,
      multi : true
    }
  ],
  entryComponents : [
    SaveStrategyComponent,
    LoadStrategyComponent,
    SignupComponent,
    LoginComponent,
    CollectionNamesComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
