//upload news image
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import{ FileUploader } from 'ng2-file-upload';
import {baseurl} from '../shared/baseurl';
import {AuthService} from '../services/auth.service';
import{Ng2ImgMaxService} from 'ng2-img-max';

@Component({
  selector: 'app-news-image-uploader',
  templateUrl: './news-image-uploader.component.html',
  styleUrls: ['./news-image-uploader.component.scss']
})
export class NewsImageUploaderComponent implements OnInit {

  @Input() imageName : string ;
  @Output() imageNameChange = new EventEmitter<string>();

  imageAddress : string;
  URL : string = baseurl + 'images/news/';

  public uploader : FileUploader = new FileUploader({
    url : baseurl + 'upload/newsimage',
    itemAlias : 'newsImage',
    authToken : "Bearer " + this.authService.getToken()
  });

  constructor(private authService : AuthService, private _ng2ImgMax : Ng2ImgMaxService) { 

  }

  ngOnInit() {
    this.imageAddress = this.URL + this.imageName;
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials =false;
    };
    this.uploader.onCompleteItem = (item : any, status : any) => {
      console.log('Uploaded File Details: ', item);
      this.imageName = item.file.name;
      this.imageNameChange.emit('images/news/' + this.imageName);
      this.imageAddress = this.URL + this.imageName;
    };

 //   this.uploader.authTokenHeader = `Authorization: bearer ${this.authService.getToken()}`;
  }
  handleUpload(event ) {
    this._ng2ImgMax.resizeImage(event.target.files[0], 200, 200).subscribe(
      result => {
        const newImage = new File([result], result.name);
        this.uploader.clearQueue();
        this.uploader.addToQueue([newImage]);
        this.uploader.uploadAll();
      },
      error => console.log(error)
    );
  }

}
