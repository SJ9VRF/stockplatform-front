import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsImageUploaderComponent } from './news-image-uploader.component';

describe('NewsImageUploaderComponent', () => {
  let component: NewsImageUploaderComponent;
  let fixture: ComponentFixture<NewsImageUploaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsImageUploaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsImageUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
