import { Component, OnInit, HostListener } from '@angular/core';
import{Strategy} from '../shared/strategy';
import{StrategyService} from '../services/strategy.service';
import{ActivatedRoute} from '@angular/router';
import{AuthService} from '../services/auth.service';
import {ApiService} from '../services/api.service';
import{ TranslocoService} from '@ngneat/transloco';

const PersianDate = require('persian-date');

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {

  plans=["lowRisk",'mediumRisk','highRisk'];
  single : any[] = [] ;
  single0 : any[] = [] ;
  single1 : any[] = [] ;
  single2 : any[] = [] ;
  single3 : any[] = [] ;
  single4 : any[] = [] ;
  single5 : any[] = [] ;
  single6 : any[] = [] ;

  investmentAmount : number;
  wholeStocks : string[];
  stocks : string[];
  selectedPlan : string = this.plans[1];
  selectedStock : string;
  startDate : string ='';
  endDate : string = '';
  startDay : number;
  endDay : number;
  run: boolean = false;
  //pie chart variables
  gradient: boolean = true;
  showLegend: boolean = false;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  legendPosition: string = 'below';
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  //this variable holds pie chart data
  view: any[] = [1000, 700];
  hCoefficient: number = 0.60;
  wCoefficient: number = 0.60;

  config = {
    drops : 'down'
  }
  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.view[1] = window.innerHeight * this.hCoefficient;
        this.view[0] = window.innerWidth * this.wCoefficient;
        console.log(this.view[0], this.view[1]);
  }
  
  constructor(private strategyService : StrategyService, private authService : AuthService,
    private apiService : ApiService , private translocoService : TranslocoService) { }

  runStrategy(){
    //convert date to unix timestamp
    let startD = this.startDate.split('-');
    let endD= this.endDate.split('-');
    console.log('sday',startD)
    console.log('eday', endD);
    this.startDay = new PersianDate([Number(startD[0]),Number(startD[1]),Number(startD[2])]).format("X");
    this.startDay = this.startDay * 1000;
    this.endDay = new PersianDate([Number(endD[0]),Number(endD[1]),Number(endD[2])]).format("X");
    this.endDay = this.endDay * 1000;
    console.log('startDay', this.startDay);
    console.log('endDay', this.endDay);
    console.log('investmentAmount', this.investmentAmount);

    //get the risk plan form select bar
    let index = this.plans.indexOf(this.selectedPlan)+1;
 

    //request to api
    this.apiService.portfolio(index, this.investmentAmount, this.startDay, this.endDay).subscribe((result) => {
      console.log('result', result);
      let i = 0;
      let single : any[] = [];

      let portfolio = result[0].chart;
      for (let basket of portfolio){
        let keys = Object.keys(basket);
 
        for(let key of keys){
          let value = basket[key].value;
          let name = this.translocoService.translate(basket[key].name);
          console.log("the name is" , name); 

          single.push({"name" : name, "value" : value});
                  //set pie chart data
        }

        this["single" + i] = {single}['single'];
        Object.assign(this , this["single" + i] );
        console.log("single number", "single"+i);
        console.log("single is", {single}['single']);
        single = [];
        i++;
/*
        let single0 : any[] = [{"name" : "کجتا" , "value" : 60},{"name" : "سیسی", "value" : 24},{"name" : "سیسیی", "value" : 16}];
        console.log("the singal fake is" , single0);
        Object.assign(this, { single0});
*/
      }



    })

    
  }
  ngOnInit(){

    let width = this.wCoefficient * window.innerWidth;
    let height = this.hCoefficient * window.innerHeight;
    this.view = [width, height]
    //load jwt and user info 
    this.authService.loadUserCredentials();
    let lang =this.translocoService.getActiveLang();
    for(let plan of this.plans){
      //translate risk plan
      plan = this.translocoService.translate(plan , {},lang);

    }

    // api must be used there
    //let single2 : any[] = [{"name" : "kachad" , "value" : 60},{"name" : "akhza", "value" : 24},{"name" : "mihan", "value" : 16}];

    //Object.assign(this, {single1})
    //Object.assign(this, {single2})

  }
/*
  this.strategyService.getCollectionNames().subscribe((CollectionNames) =>{
    this.wholeStocks = CollectionNames;
    this.stocks = this.wholeStocks;
  })
  searchStock(searchWord : string){
    if(searchWord){
      this.stocks=this.wholeStocks.filter((stock) => stock.includes(searchWord));
    }
    else{
      this.stocks = this.wholeStocks;
    }
  }



*/
onSelect(data): void {
  console.log('Item clicked', JSON.parse(JSON.stringify(data)));
}

onActivate(data): void {
  console.log('Activate', JSON.parse(JSON.stringify(data)));
}

onDeactivate(data): void {
  console.log('Deactivate', JSON.parse(JSON.stringify(data)));
}

}
