import{ProfileComponent} from '../profile/profile.component';

import { AuthGuardService as AuthGuard } from '../services/auth-guard.service';
import{Routes} from '@angular/router';
import { StrategyComponent } from '../strategy/strategy.component';
import { HomeComponent } from '../home/home.component';
import { NewsComponent } from '../news/news.component';
import { HeaderComponent } from '../header/header.component';
import { NewsDetailComponent } from '../news-detail/news-detail.component';
import { MakeNewsComponent } from '../make-news/make-news.component';
import { DigitalCurrencyComponent } from '../digital-currency/digital-currency.component';
import { TradingViewComponent } from '../trading-view/trading-view.component';
import { CreditComponent } from '../credit/credit.component';
import { ShopComponent } from '../shop/shop.component';
import { StrategyOutputComponent } from '../strategy-output/strategy-output.component';
import { TestPackageComponent } from '../test-package/test-package.component';
import { AuthenticationComponent } from '../authentication/authentication.component';
import { IdentityConfirmationComponent } from '../identity-confirmation/identity-confirmation.component';
import { AiEngineComponent } from '../ai-engine/ai-engine.component';
import { PortfolioComponent } from '../portfolio/portfolio.component';
import { TourComponent } from '../tour/tour.component';
import { TutorialComponent } from '../tutorial/tutorial.component';
import { AdforumComponent } from '../adforum/adforum.component';
import { AboutusComponent } from '../aboutus/aboutus.component';
import { ContactusComponent } from '../contactus/contactus.component';

export const routes : Routes = [
    {path : '', redirectTo : 'home', pathMatch: 'full'},
    {path: 'home', component : HomeComponent},
    {path: 'news', component : NewsComponent},
    {path: 'usersProfile',  component : ProfileComponent},
    {path : 'strategy', component : StrategyComponent,canActivate: [AuthGuard]},
    {path :'header' , component : HeaderComponent},
    {path : 'newsDetail/:newsId', component: NewsDetailComponent},
    {path: 'makeNews', component: MakeNewsComponent},
    {path : 'digitalCurrency', component : DigitalCurrencyComponent, canActivate: [AuthGuard] },
    {path : 'trading-View', component:TradingViewComponent},
    {path : 'credit/:wallet', component : CreditComponent, canActivate: [AuthGuard] },
    {path : 'shop', component:ShopComponent, canActivate: [AuthGuard] },
    {path : 'strategyOutput', component : StrategyOutputComponent,canActivate: [AuthGuard]},
    {path : 'testPackage/:packageName', component : TestPackageComponent, canActivate : [AuthGuard]},
    {path : 'auth/:userId', component : AuthenticationComponent , canActivate : [AuthGuard]},
    {path : 'idnConfirm', component : IdentityConfirmationComponent, canActivate :[AuthGuard]},
    {path : "aiEngine", component : AiEngineComponent, canActivate : [AuthGuard]},
    {path: "portfolio", component : PortfolioComponent,canActivate: [AuthGuard]},
    {path: "tour", component : TourComponent},
    {path: "adforum", component : AdforumComponent,canActivate: [AuthGuard]},
    {path: "contactus", component : ContactusComponent},
    {path: "aboutus", component : AboutusComponent},
]