import { Injectable } from '@angular/core';
import{ HttpClient } from '@angular/common/http';
import{ ProcessHttpErrorService } from './process-http-error.service';
import{ baseurl } from '../shared/baseurl';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import{News} from '../shared/news';
import{TranslocoService} from '@ngneat/transloco';

interface news{
  title:string,
  content : string,
  image: string
}
@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private processHttpErrorService : ProcessHttpErrorService,
    private http : HttpClient, private translocoService : TranslocoService) { }

  publish(news : news): Observable<any>{
    return this.http.post(baseurl + 'news',news)
    .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
  getNews() : Observable<News[]>{
    let lang = this.translocoService.getActiveLang();
    return this.http.get<News[]>(baseurl + 'news/lang/'+lang)
    .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
  getNewsById(_id : string): Observable<News> {
    let lang = this.translocoService.getActiveLang();
    return this.http.get<News>(baseurl + 'news/'+_id+'/lang/'+ lang)
    .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
  deleteNewsById(_id : string) : Observable<any> {
      return this.http.delete(baseurl+'news/'+ _id)
      .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
  updateNewsById(_id : string, news : News) : Observable<any>{
    return this.http.put(baseurl+ 'news/' + _id , news)
    .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
  updateNewsComment(newsId : string ,commentId: string, newComment: string): Observable<any>{
    return this.http.put(baseurl+ 'news/'+newsId+ '/comments/'+ commentId, {content: newComment})
    .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
  postNewsComment(_id : string, content : string): Observable<News>{
    return this.http.post<News>(baseurl + 'news/' + _id + '/comments', {content : content})
    .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
  deleteCommentById(newsId : string, commentId : string): Observable<News>{
    return this.http.delete<News>(baseurl + 'news/' + newsId + '/comments/'+ commentId)
    .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
}
