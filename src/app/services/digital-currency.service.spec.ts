import { TestBed } from '@angular/core/testing';

import { DigitalCurrencyService } from './digital-currency.service';

describe('DigitalCurrencyService', () => {
  let service: DigitalCurrencyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DigitalCurrencyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
