import { Injectable } from '@angular/core';
import{HttpErrorResponse} from '@angular/common/http';
import {throwError } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ProcessHttpErrorService {

  constructor() { }

  handleRespError(error : HttpErrorResponse | any) {
    let errMessage;
    console.log('err from handler', error);
    if(error.error instanceof ErrorEvent){
      console.log('here' , error);
      errMessage = error.error.message;
    }
    else {
      console.log('there', error);
      errMessage = `${error.status} - ${error.message}`;
    }
    return throwError(errMessage);
  }
}
