import { Injectable } from '@angular/core';
import{baseurl} from '../shared/baseurl';
import{Observable, Subject} from 'rxjs';
import {map, catchError, last} from 'rxjs/operators';
import { HttpResponse , HttpClient } from '@angular/common/http';
import {ProcessHttpErrorService} from './process-http-error.service';
import {User} from '../shared/user';

interface authResponse {
    success : boolean,
    status : boolean,
    token : string,
    admin : boolean
}
interface user{
  email : string,
  password : string
}
@Injectable({
  providedIn: 'root'
})


export class AuthService {

  tokenKey = 'JWT';
  isAuthenticated : boolean;
  authToken : string = undefined;
  username : Subject<string> = new Subject<string>();
  visibility : Subject<boolean> = new Subject<boolean>();
  admin : Subject<boolean> = new Subject<boolean>();
  email : string;

  constructor(private http : HttpClient, private processHTTPError : ProcessHttpErrorService) { }
  
  logIn(user : any) : Observable<any>{
    return this.http.post<authResponse>(baseurl + 'users/login', 
    {email : user.email , password : user.password})
    .pipe(map(resp => {
      this.storeUserCredentials({email : user.email , token: resp.token, admin : resp.admin});
      return ({'success' : true , 'email' : user.email});
    }) 
    ,catchError(error => this.processHTTPError.handleRespError(error) ));
    
  }
  storeUserCredentials(credentials : any){
    localStorage.setItem(this.tokenKey , JSON.stringify(credentials));
    let cred = JSON.parse(localStorage.getItem(this.authToken));
    console.log('from storeCred', cred);
    this.UseCredentials(credentials);
  }
  UseCredentials(credentials : any){
    this.isAuthenticated = true;
    this.authToken = credentials.token;
    this.email = credentials.email;
    console.log('auth token from auth service' , this.authToken);
    this.sendUsername(credentials.username);
    this.sendVisibility(true);
    this.sendAdmin(credentials.admin);  
  }
  sendUsername(username : string){
    this.username.next(username);
  }
  loadUserCredentials(){
    console.log('heyyyyy')
    let credentials = JSON.parse(localStorage.getItem(this.tokenKey));
    if(credentials && credentials.email !== undefined){
      this.UseCredentials(credentials);
    }
  }
  signUp(user : user) : Observable<any>{
    console.log('user',user);
    return this.http.post(baseurl + 'users/signup', user)
    .pipe(catchError(error => this.processHTTPError.handleRespError(error)));
  }
  logOut(){
    this.destroyUserCredentials();
  }
  destroyUserCredentials(){
    localStorage.removeItem(this.tokenKey);
    this.isAuthenticated = false;
    this.authToken = undefined;
    this.admin.next(false);
    this.clearUsername();
    this.clearVisibility();
  }
  clearUsername(){
    this.username.next(undefined);
  }
  isLoggedIn() : boolean{
    return this.isAuthenticated;
  }
  getUsername() : Observable<string> {
    return this.username.asObservable();
  }
  getToken() : string {
    return this.authToken;
  }
  getProfileInformation(email? : string) : Observable<any>{
    return this.http.get<User>(baseurl +'users/profile')
    .pipe(catchError((err) => this.processHTTPError.handleRespError(err)));
  }
  sendVisibility(visibility : boolean){
    console.log('vis', visibility);
    return this.visibility.next(true);
  }
  getVisibility(): Observable<boolean>{
    return this.visibility.asObservable();
  }
  clearVisibility(){
    return this.visibility.next(false);
  }
  sendAdmin(isAdmin : boolean){
    this.admin.next(isAdmin);
  }
  getAdmin(){
    return this.admin.asObservable();
  }
  loadAdmin(){
    return this.admin.pipe(last());
  }
  getEmail(): string {
    return this.email;
  }
}
