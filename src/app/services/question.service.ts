import { Injectable } from '@angular/core';
import{ QuestionBase } from '../shared/question/question-base';
import { TextBoxQuestion } from '../shared/question/question-textbox';
import { DropdownQuestion } from '../shared/question/question-dropdown';
import {MAquestions, MACDquestions, Comparisonquestions, Crossquestions,
   PriceDataQuestion, Signalquestions, RSIquestions, ADXquestions, WilliamsRquestions
  , ATRquestions, MFIquestions, CCIquestions, OBVquestions,Bollingerquestions, stochRSIquestions,
   Ichimokuquestions, commonMathquestions, Stochasticquestions} from '../shared/question/questions';

import { of } from 'rxjs';
import { MFI, bollingerbands } from 'technicalindicators';
import{DiagramNode} from '../shared/Diagramnode';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  questions : QuestionBase<string | number> [];
  constructor() { }


 /* this.questionService.getQuestion(node.name).subscribe((questions) => {
    for(let question of questions){
      if(node[question.key]){
        console.log(question.key,':',node[question.key]);
        question.selected = node[question.key];
      }
      else{
      
      }
    }
    this.questions = questions
    console.log('nodequestion', this.questions);
  });*/
  getQuestion(node : DiagramNode){
    switch(node.name){
      case 'Stochastic':
        this.questions = Stochasticquestions;
        break;
      case 'commonMath': 
        this.questions = commonMathquestions;
        break;
      case 'Ichimoku':
        this.questions = Ichimokuquestions;
        break;
      case 'StochRsi':
        this.questions = stochRSIquestions;
        break;
      case 'BollingerBands':
        this.questions = Bollingerquestions;
        break;
      case 'OBV':
        this.questions = OBVquestions;
        break;
      case 'CCI':
        this.questions = CCIquestions;
        break;
      case 'MFI':
        this.questions = MFIquestions;
        break;
      case 'MA':
        this.questions= MAquestions;
        break;
      case  'MACD' :
        this.questions = MACDquestions;
        console.log('MACDEnter', MAquestions , 'questions' , this.questions);
        break;
      
      case 'Comparison':
        this.questions = Comparisonquestions;
        break;

      case 'Cross':
        this.questions = Crossquestions;
        break;
        case 'Signal':
          this.questions = Signalquestions;
          break;
      case 'PriceData':
        this.questions =  PriceDataQuestion;
        break;
      case 'RSI':
        this.questions = RSIquestions;
        break;
      case 'ADX':
        this.questions = ADXquestions;
        break;
      case 'Williams %R':
        this.questions = WilliamsRquestions
        break;
        case 'ATR':
          this.questions = ATRquestions;
          break;
      default:
        break;
    }

    console.log('questionservice | questions : ' , this.questions);
    for(let question of this.questions){
      console.log('nodeProperty',question.key,':',node[question.key]);
      console.log('node', node);
      console.log('timeframe', node.timeframe);
      if(node[question.key]){
        console.log('before :',question.key +  ':' + question.selected);
        question.selected = node[question.key];
        console.log('after :',question.key +  ':' + question.selected);
      }
    }
    console.log('this.questions',this.questions);
    return of(this.questions.sort((a, b) => a.order - b.order));

  }
}
