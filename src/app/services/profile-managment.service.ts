import { Injectable, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import{baseurl} from '../shared/baseurl';
import{Observable, Subject} from 'rxjs';
import{catchError, map, last, takeLast} from 'rxjs/operators';
import{ProcessHttpErrorService} from './process-http-error.service';
import{AuthService} from './auth.service';
import{AvatarModule} from 'ngx-avatar';
import{User} from '../shared/user';

@Injectable({
  providedIn: 'root'
})
export class ProfileManagmentService implements OnInit {

  imageName : Subject<string> = new Subject<string>();

  constructor(private http : HttpClient,
    private processHttpErrorService : ProcessHttpErrorService,
    private authService : AuthService) { }

  ngOnInit(){
    console.log('enter onInit')
    this.authService.loadUserCredentials();
  }
  updateUserProfile(profile : any, id?: string): Observable<any>{
    profile.id = id;
    return this.http.put(baseurl + 'users/profile',profile)
    .pipe(catchError(err => this.processHttpErrorService.handleRespError(err)));
  }
  getUserProfile(): Observable<any>{
    return this.http.get(baseurl + 'users/profile')
    .pipe(map((user) => {
      this.setImageName(user['image']);
      return user;
    }),catchError(err => this.processHttpErrorService.handleRespError(err)))
  }
  changePassword(password : string) : Observable<any>{
      return this.http.post(baseurl + 'users/resetpassword', {password : password})
      .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
  changeProfileImage(imageName: string): Observable<any>{
    return this.http.put(baseurl + 'users/changeProfileImage',{image : imageName})
    .pipe(map((resp) => {
      this.setImageName(imageName);
      return resp;
    }),catchError((err) =>  this.processHttpErrorService.handleRespError(err)));

  }
  setImageName(imageName : string){
    this.imageName.next(imageName);
  }
  sendImageName(){
    return this.imageName.asObservable();
  }
  loadImageName(): any{
    return this.imageName.pipe(takeLast(1));
  }
  getProfileImage(): Observable<any>{
    return this.http.get(baseurl + 'users/getProfileImage')
    .pipe(map((resp) => {
      this.setImageName(resp['image']);
      return resp;
    }),catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
  getUserByAdmin(id : string): Observable<any>{
    return this.http.get(baseurl + 'users/detail/'+id)
    .pipe(catchError(err => this.processHttpErrorService.handleRespError(err)));
  }
  getUsersByAdmin(): Observable<any>{
    return this.http.get(baseurl + 'users')
    .pipe(catchError(err => this.processHttpErrorService.handleRespError(err)));
  }
}
