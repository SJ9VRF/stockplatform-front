import { TestBed } from '@angular/core/testing';

import { ProfileManagmentService } from './profile-managment.service';

describe('ProfileManagmentService', () => {
  let service: ProfileManagmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProfileManagmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
