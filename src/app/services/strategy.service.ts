import { Injectable, Output } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {baseurl} from '../shared/baseurl';
import{DiagramNode} from '../shared/Diagramnode';
import {DiagramLink} from '../shared/DiagramLinks';
import{Strategy} from '../shared/strategy';
import{Observable} from 'rxjs';
import{ProcessHttpErrorService} from './process-http-error.service';
import{catchError} from 'rxjs/operators';
import{OutPut} from '../shared/output';
import { User } from '../shared/user';

@Injectable({
  providedIn: 'root'
})
export class StrategyService {

  strategyOutPut : OutPut[] = [];

  constructor(private http : HttpClient, private processHttpService : ProcessHttpErrorService) { }


  saveStrategy(strategy : Strategy) : Observable<Strategy>{
      return this.http.post<Strategy>(baseurl + 'strategies' , strategy)
      .pipe(catchError((err) => this.processHttpService.handleRespError(err)));
  }
  loadStrategy(name : string): Observable<Strategy> {
    return this.http.get<Strategy>(baseurl + 'strategies/' + name)
    .pipe(catchError((err) => this.processHttpService.handleRespError(err)));
  }
  getPriceData(timeframe : string, practicalPrice : string ,collectionName?: string, startDate ? : number
    ,endDate? : number): Observable<any> {
    return this.http.post(baseurl+'strategies/exchangeData/'+timeframe , 
    {collectionName : collectionName, practicalPrice : practicalPrice, startDate:startDate, endDate : endDate})
    .pipe(catchError((err) => this.processHttpService.handleRespError(err)));
  }
  getDateVolume(collectionName?: string, startDate ? : number
    ,endDate? : number): Observable<any> {
      return this.http.post(baseurl+'strategies/dateVolume/' , 
      {collectionName : collectionName, practicalPrice : 'Volume', startDate:startDate, endDate : endDate})
      .pipe(catchError((err) => this.processHttpService.handleRespError(err)));
    }
  getCollections() : Observable<any>{
    return this.http.get(baseurl+'strategies/collection/collections/')
    .pipe(catchError((err) => this.processHttpService.handleRespError(err)));
  }
  getCollectionNames() : Observable<any>{
    return this.http.get(baseurl+'strategies/collection/collectionNames/')
    .pipe(catchError((err) => this.processHttpService.handleRespError(err)));
  }
  forSell(_id : string, price : number):Observable<any>{
    return this.http.put(baseurl+'strategies/forSell/'+_id,{price : price, forSell : true})
    .pipe(catchError((err) => this.processHttpService.handleRespError(err)));
  }
  getForSellStrategies():Observable<any> {
    return this.http.get(baseurl+'strategies/forSell/strategy?forSell=true')
    .pipe(catchError((err) => this.processHttpService.handleRespError(err)));
  }
  setStrategyOutPut(output : OutPut[]){
      this.strategyOutPut = output;
  }
  getStrategyOutPut(): OutPut[]{
    return this.strategyOutPut;
  }
  getStrategy() : Observable<Strategy[]>{
    return this.http.get<Strategy[]>(baseurl+'strategies')
    .pipe(catchError((err) => this.processHttpService.handleRespError(err)));
  }
  purchase(name :  string): Observable<User> {
    return this.http.post<User>(baseurl + 'strategies/st/purchased', {purchasedName : name})
    .pipe(catchError((err) => this.processHttpService.handleRespError(err)));
  }

}
