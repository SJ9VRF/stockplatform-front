import {Injectable , Injector} from '@angular/core';
import {HttpClient, HttpEvent, HttpInterceptor, HttpRequest , HttpHandler} from '@angular/common/http';
import {AuthService} from './auth.service';
import{Observable} from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private inj : Injector){}

    intercept(req : HttpRequest<any> , next : HttpHandler): Observable<HttpEvent<any>> {
        console.log('enter to intercept');
        const authService = this.inj.get(AuthService);
        const authToken = authService.getToken();
        console.log('authtoken', authToken);
        console.log('req', req);
        const authReq = req.clone({headers: req.headers.set('Authorization', 'bearer ' + authToken)});
        return next.handle(authReq);
    }
}