import { Injectable } from '@angular/core';
import{ HttpClient, HttpParams } from '@angular/common/http';
import{ ProcessHttpErrorService } from './process-http-error.service';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiURL : string = "http://localhost:5000/api/v1";
  constructor(private processHttpErrorService : ProcessHttpErrorService,
    private http : HttpClient) { }

  predictor(name? :string, time_start? : number, time_end? : number): Observable<any>{
    let params = new HttpParams();
    params.append('name' , name.toString());
    params.append('time_start', time_start.toString());
    params.append('time_end', time_end.toString());
    console.log('predictoer')
    return this.http.get(this.apiURL + `/predictor/stock?id=${name}&time_start=${time_start}&time_end=${time_end}`)
    .pipe(catchError((err) => this.processHttpErrorService.handleRespError(err)));
  }
  portfolio(risk? :number, investment_amount? : number, time_start? : number, time_end? : number): Observable<any>{
    return this.http.get(this.apiURL + `/portfolio/stock?plan_type=${risk}&investment_amount=${investment_amount}&time_start=${time_start}&time_end=${time_end}`)
    .pipe(catchError((err) => this.processHttpErrorService.handleRespError(err)));
  }
}
