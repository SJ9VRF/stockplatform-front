import { TestBed } from '@angular/core/testing';

import { ProcessHttpErrorService } from './process-http-error.service';

describe('ProcessHttpErrorService', () => {
  let service: ProcessHttpErrorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProcessHttpErrorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
