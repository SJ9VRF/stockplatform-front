import { Injectable } from '@angular/core';
import{ HttpClient } from '@angular/common/http';
import{ ProcessHttpErrorService } from './process-http-error.service';
import{ baseurl } from '../shared/baseurl';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Traders } from '../shared/traders';
import { User} from '../shared/user';

interface Order {
  unitPrice : number,
  value: number
}

@Injectable({
  providedIn: 'root'
})

export class DigitalCurrencyService {

  constructor(private processHttpErrorService : ProcessHttpErrorService,
    private http : HttpClient) { }

  getSellers(currencyName : string) : Observable<Traders[]>{
    return this.http.get<Traders[]>(baseurl + 'currency/sellersList/' + currencyName)
    .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
  getShopper(currencyName : string) : Observable<Traders[]>{
    return this.http.get<Traders[]>(baseurl + 'currency/shoppersList/' + currencyName)
    .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
  /*** /shoppersList/:currencyName/orders */
 postSellers(currencyName : string , order : Order): Observable<any>{
    return this.http.post(baseurl + 'currency/sellersList/'+ currencyName+ '/orders', order)
    .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
 }
 postShoppers(currencyName : string , order : Order): Observable<any>{
    return this.http.post(baseurl + 'currency/shoppersList/'+ currencyName+ '/orders', order)
    .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
 }
  getWallet() : Observable<any> {
    return this.http.get(baseurl + 'users/wallet')
    .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
  getDigitalWallet(currencyName : string) : Observable<any> {
    return this.http.get(baseurl + 'users/digitalWallet/' + currencyName)
    .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
  putWallet(wallet : number): Observable<any>{
      return this.http.put(baseurl + 'users/wallet', {wallet : wallet })
      .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
  putDigitalWallet(currencyName : string, value : number){
    return this.http.put(baseurl + 'users/digitalWallet/'+ + currencyName, { value : value})
    .pipe(catchError((err) =>  this.processHttpErrorService.handleRespError(err)));
  }
}
