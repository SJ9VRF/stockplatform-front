import { Component, OnInit, SimpleChanges, ViewChild, ChangeDetectorRef  } from '@angular/core';
import{DigitalCurrencyService} from '../services/digital-currency.service';
import { FormControl, Validators } from '@angular/forms';
import { subscribeOn } from 'rxjs/operators';
import{TranslocoService} from  '@ngneat/transloco';

declare const TradingView: any;

@Component({
  selector: 'app-digital-currency',
  templateUrl: './digital-currency.component.html',
  styleUrls: ['./digital-currency.component.scss']
})
export class DigitalCurrencyComponent implements OnInit {
  currencyNames : string [] = ['bitcoin', 'ethereum', 'XRP'];
  currencyName : string = 'bitcoin' ;
  baseUnit : string;
  baseUnits : string[] = ['Day','Month','week'];
  wallet : number;
  digitalWallet : number;
  current : string = this.currencyName;
  order = {unitPrice : 0 , value : 0};
  isChecked : boolean = true //when is true = sell
  refresh : boolean = false;
  searchText : string;
  course : any;

  @ViewChild('select') selectChild;

  constructor(private digitalCurrencyService : DigitalCurrencyService,
    private translocoService : TranslocoService, private cdref: ChangeDetectorRef) { }


  ngOnInit() {
   this.baseUnits = [this.translocoService.translate('day'),this.translocoService.translate('month'),
    this.translocoService.translate('week')];
    this.digitalCurrencyService.getWallet().subscribe((wallet) => {
     this.wallet = wallet.wallet;
    })
    this.digitalCurrencyService.getDigitalWallet(this.currencyName).subscribe((digitalWallet) => {
      this.digitalWallet = digitalWallet.value;
    })
    this.course = [
      { id: 1, name: 'HTML' },
      { id: 2, name: 'CSS' },
      { id: 3, name: 'JAVASCRIPT' },
      { id: 4, name: 'ANGULAR' },
      { id: 5, name: 'REACT' },
      { id: 6, name: 'NODEJS' },
      { id: 7, name: 'PHP' },
      { id: 8, name: 'MYSQL' }
    ]
  }
  ngAfterViewInit(){
    new TradingView.widget(
      {
      "width": 980,
      "height": 610,
      "symbol": "BTCUSD",
      "timezone": "Etc/UTC",
      "theme": "Light",
      "style": "1",
      "locale": "en",
      "toolbar_bg": "#f1f3f6",
      "enable_publishing": false,
      "withdateranges": true,
      "range": "ytd",
      "hide_side_toolbar": false,
      "allow_symbol_change": true,
      "show_popup_button": true,
      "popup_width": "1000",
      "popup_height": "650",
      "no_referral_id": true,
      "container_id": "tradingview_bac65"
    }
      );

  }
  //check the select bar and get the specific currency data when the selected currency name is changed
 ngAfterViewChecked(){
  this.baseUnits = [this.translocoService.translate('day'),this.translocoService.translate('month'),
  this.translocoService.translate('week')];
   if(this.selectChild.value !== this.current){
    this.digitalCurrencyService.getDigitalWallet(this.currencyName).subscribe((digitalWallet) => {
      this.digitalWallet = digitalWallet.value;
    })
    this.current = this.currencyName;
  }
  this.cdref.detectChanges();

    /*this.digitalCurrencyService.getDigitalWallet(this.currencyName).subscribe((digitalWallet) => {
      this.digitalWallet = digitalWallet.value;
    })*/
  }
  sendOrder(){
      this.refresh = false;
      if(this.isChecked){
        this.digitalCurrencyService.postSellers(this.currencyName, this.order).subscribe((sellers) => {
          this.refresh = true;
          console.log('sellers', sellers)
        })
      }
      else{
        this.digitalCurrencyService.postShoppers(this.currencyName, this.order).subscribe((shoppers) => {
          this.refresh = true;
          console.log('shoppers',shoppers)
        })      }
  }
}
