import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MakeNewsComponent } from './make-news.component';

describe('MakeNewsComponent', () => {
  let component: MakeNewsComponent;
  let fixture: ComponentFixture<MakeNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MakeNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MakeNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
