//edit news text in text editor ans set image for news
import { Component, OnInit, createPlatformFactory, HostListener } from '@angular/core';
import{AuthService} from '../services/auth.service';
import{ AngularEditorConfig } from '@kolkov/angular-editor';
import{ NewsService } from '../services/news.service';
import{baseurl} from '../shared/baseurl';

interface news {
  title : string,
  content: string,
  image: string
}

@Component({
  selector: 'app-make-news',
  templateUrl: './make-news.component.html',
  styleUrls: ['./make-news.component.scss']
})
export class MakeNewsComponent implements OnInit {

  admin: boolean = false;
  //htmlContent = '';
  makeNewsForm : news = {image : '', title : '', content: ''};
  URL : string = baseurl + '/images/news';
  imageName : string = 'defualtImageNewsx.jpg';
  imageAddress : string = this.URL + this.imageName;
  title : string= '';
  content:string= '';

  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '300px',
      minHeight: '0',
      maxHeight: 'auto',
      width: '900px',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
  //  uploadUrl: 'http://localhost:3000/upload/newsImage',
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      [
        'fontName'
      ],
      [
        'customClasses',
      ]
    ]
    
    
};

@HostListener('window:resize', ['$event'])
getScreenSize(event?) {
      this.editorConfig.height =  `${30}vh`;
      this.editorConfig.width = `${60}vw`
      this.editorConfig.defaultFontSize = `${2}vw`
      console.log('editorConfig', this.editorConfig);
}
  constructor(private authService : AuthService, private newsService : NewsService) { }

  ngOnInit() {
    console.log('imageName', this.imageName);
    this.editorConfig.height = `${30}vh`;
    this.editorConfig.width = `${60}vw`
    this.editorConfig.defaultFontSize = `${2}vw`
  }




  

  publish(){
    console.log('ppppppp',this.makeNewsForm);
    this.makeNewsForm.image =  this.imageName;
    this.makeNewsForm.content = this.content;
    this.makeNewsForm.title = this.title;
    console.log('sdjkdbkgdfg');
    this.newsService.publish(this.makeNewsForm).subscribe((news) => {
      console.log('news' , news);
    })
  }
  imageChange(imageName : string){
    this.imageName = imageName;
    this.imageAddress = this.URL + this.imageName;
    console.log('newsimageadress', this.imageName);
  }
}
