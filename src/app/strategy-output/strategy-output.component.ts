import {Component, OnInit, ViewChild, Input, SimpleChange, SimpleChanges} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import{DigitalCurrencyService} from '../services/digital-currency.service';
import{ActivatedRoute} from '@angular/router';
import{StrategyService} from '../services/strategy.service';
var persianDate = require('persian-date');

export interface TraderData {
  symbol : string,
  startDate : string,
  tradeDays: number,
  endDate : string,
  profit : number,
  succTrades : number,
  unsuccTrades : number,
  trades : number,
  color : string
}


@Component({
  selector: 'app-strategy-output',
  templateUrl: './strategy-output.component.html',
  styleUrls: ['./strategy-output.component.scss']
})
export class StrategyOutputComponent implements OnInit {

  displayedColumns: string[] = ['symbol','startDate','endDate','tradeDays','profit','succTrades','unsuccTrades','trades']
  symboles : string[] = []
  dataSource: MatTableDataSource<TraderData>;
  outputs : any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor( private digitalCurrencyService : DigitalCurrencyService,private route : ActivatedRoute,
    private strategyService : StrategyService) { }

  ngOnInit() {
    //get strategy output from strategyservice
    this.outputs = this.strategyService.getStrategyOutPut();
    for(let output of this.outputs){
      //get stock names
      if(output.profit.length > 0){ 
        this.symboles.push(output.name);
      }
    }
    console.log('symbols', this.symboles);
    console.log('outputtt',this.outputs)
  }
  ngAfterViewInit(){
    this.getData();
  }
  getData(){
      let TraderInfos : TraderData[] =[];
      for(let symbol of this.symboles){
        let traderData : TraderData = {
          symbol : '',
          startDate : '',
          tradeDays: 0,
          endDate : '',
          profit : 0,
          succTrades : 0,
          unsuccTrades : 0,
          trades : 0,
          color : ''
        };

        traderData.symbol = symbol;
        traderData.tradeDays = this.Volume(symbol);
        traderData.startDate = this.startDate(symbol);
        traderData.endDate = this.endDate(symbol);
        traderData.profit = this.profit(symbol);
        if(traderData.profit > 0){
          traderData.color = 'green';
        }
        else {
          traderData.color = 'red';
        }
        traderData.trades = this.tradesNumber(traderData.symbol);
        [traderData.succTrades , traderData.unsuccTrades] = this.succTrades(traderData.symbol);
        TraderInfos.push(traderData);
      }
      console.log('info',TraderInfos);
      this.dataSource = new MatTableDataSource(TraderInfos);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
  }

  Volume(symbol : string){

    let output = this.outputs.find((output) => (output.name === symbol));
    return output.volume;
  }
  //convert unix timestamp to persian date
  startDate(symbol : string){
    let output = this.outputs.find((output) => (output.name === symbol));
    var startDate = new persianDate().unix(output.startDate/1000);
    var day = startDate.State.persianAstro.day;
    var month = startDate.State.persianAstro.month + 1;
    var year = startDate.State.persianAstro.year;
    console.log('endDate',startDate.State.persianAstro)
    return `${year}/${month}/${day}`;
  }
    //convert unix timestamp to persian date
  endDate(symbol : string){
    let output = this.outputs.find((output) => (output.name === symbol));
    var endDate = new persianDate().unix(output.endDate / 1000);
    var day = endDate.State.persianAstro.day;
    var month = endDate.State.persianAstro.month+1;
    var year = endDate.State.persianAstro.year;
    console.log('endDate',endDate.State.persianAstro)
    return `${year}/${month}/${day}`;
  }
  //calculate profit
  profit(symbol : string){
    let output = this.outputs.find((output) => (output.name === symbol));
    let sum = 0;
    for(let p of output.profit){
      sum += p;
    }
    return Number((sum/ output.profit.length).toFixed(2));
  }
  tradesNumber(symbol : string){
    let output = this.outputs.find((output) => (output.name === symbol));
    return  output.profit.length;
  }
  //claculate success trades and unsuccess trades
  succTrades(symbol : string){
    let output = this.outputs.find((output) => (output.name === symbol));
    let succTrades = 0;
    let unsuccTrades = 0;
    for(let p of output.profit){
      if(p > 0){
        succTrades++;
      }
      else{
        unsuccTrades++;
      }
    }
    return [succTrades,unsuccTrades];
  }

}
