import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StrategyOutputComponent } from './strategy-output.component';

describe('StrategyOutputComponent', () => {
  let component: StrategyOutputComponent;
  let fixture: ComponentFixture<StrategyOutputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StrategyOutputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StrategyOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
