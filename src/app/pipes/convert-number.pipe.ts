import { Pipe, PipeTransform } from '@angular/core';
import {TranslocoService } from '@ngneat/transloco';
var persianDate = require ('persian-date');

@Pipe({
  name: 'convertNumber'
})
export class ConvertNumberPipe implements PipeTransform {
  constructor(private translocoService : TranslocoService){
  }
  transform(value: string): string {
    let activeLang = this.translocoService.getActiveLang();
    if(activeLang === "en"){
      persianDate.toLocale('en');
      let year = value.split('/')[0];
      let month = value.split('/')[1];
      let day = value.split('/')[2];
      let date = new persianDate().toCalendar('en').format("YYYY/MM/DD");  
      console.log('date',date);
      return date;
    }
    else{
      return value
    }

  }
  /*persianNumbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
  englishNumbers = ['0','1','2','3','4','5','6','7','8','9'];
  constructor(private translocoService : TranslocoService){
  }
  transform(value: string): string {
    let activeLang = this.translocoService.getActiveLang();
    let numbers = value.toString().split('');
    for(let number of numbers){
      if(activeLang === "en"){
        let enIndex =this.englishNumbers.indexOf(number.toString());
        let faIndex = this.persianNumbers.indexOf(number.toString());
        if(enIndex >= 0){
          console.log('number1', number)
          number =  number.toString();
        }
        else{
          number   = (this.englishNumbers[faIndex]);
          console.log('number2',number);
        }
      }
      else{
        let enIndex =this.englishNumbers.indexOf(number.toString());
        let faIndex = this.persianNumbers.indexOf(number.toString());
        if(faIndex >= 0){
          number = number.toString();
        }
        else{
          number = (this.persianNumbers[enIndex]);
        }
      } 
    }
    let lastNumber = ''
    for(let  number of numbers){
      lastNumber += number;
    }
    console.log('lastNumber', lastNumber);
    return lastNumber;
  }
*/
}
