import { Component, OnInit } from '@angular/core';
import{Strategy} from '../shared/strategy';
import{StrategyService} from '../services/strategy.service';
import{ActivatedRoute} from '@angular/router';
import { shareReplay } from 'rxjs/operators';

const PersianDate = require('persian-date');

@Component({
  selector: 'app-test-package',
  templateUrl: './test-package.component.html',
  styleUrls: ['./test-package.component.scss']
})
export class TestPackageComponent implements OnInit {

  wholeStrategies : Strategy[];
  strategies : Strategy[];
  wholeStocks : string[];
  stocks : string[];
  selectedPackage : string = '';
  selectedStock : string;
  startDate : string ='';
  endDate : string = '';
  startDay : number;
  endDay : number;
  run: boolean = false;
  config = {
    drops : 'down'
  }
  constructor(private strategyService : StrategyService, private route : ActivatedRoute) { }

  runStrategy(){
    //convert date to timestmap
    let startD = this.startDate.split('-');
    let endD= this.endDate.split('-');
    console.log('sday',startD)
    console.log('eday', endD);
    this.startDay = new PersianDate([Number(startD[0]),Number(startD[1]),Number(startD[2])]).format("X");
    this.startDay = this.startDay * 1000;
    this.endDay = new PersianDate([Number(endD[0]),Number(endD[1]),Number(endD[2])]).format("X");
    this.endDay = this.endDay * 1000;
    console.log('startDay', this.startDay);
    console.log('endDay', this.endDay);
    this.run = true;
  }
  ngOnInit(): void {
    //set selectedPackage with  packageName in params if it exists
    if(this.route.snapshot.params.packageName !== "all"){
      console.log('params' , this.route.snapshot.params.packageName)
      this.selectedPackage = this.route.snapshot.params.packageName;
      console.log('selectedPackage', this.selectedPackage);
    }
    this.strategyService.getForSellStrategies().subscribe((strategy) => {
      this.wholeStrategies = strategy;
      this.strategies = this.wholeStrategies;
    });
    this.strategyService.getCollectionNames().subscribe((CollectionNames) =>{
      this.wholeStocks = CollectionNames;
      this.stocks = this.wholeStocks;
    })
  }
  searchPackage(searchWord : string){
    console.log('keuup', searchWord)
    if(searchWord){
      this.strategies=this.wholeStrategies.filter((str) => str.Name.includes(searchWord));
    }
    else{
      this.strategies = this.wholeStrategies;
    }
  }
  searchStock(searchWord : string){
    if(searchWord){
      this.stocks=this.wholeStocks.filter((stock) => stock.includes(searchWord));
    }
    else{
      this.stocks = this.wholeStocks;
    }
  }

}
