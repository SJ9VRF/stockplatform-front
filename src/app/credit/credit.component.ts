import { Component, OnInit } from '@angular/core';
import {ActivatedRoute , Params} from '@angular/router';
import{DigitalCurrencyService} from '../services/digital-currency.service';
@Component({
  selector: 'app-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.scss']
})
export class CreditComponent implements OnInit {

  wallet : number;
  increament : number;

  constructor(private route : ActivatedRoute, 
    private digitalCurrencyService : DigitalCurrencyService) { }

  ngOnInit() {
    this.route.params.subscribe((params : Params) => {
      console.log('params',params);
      this.wallet = params['wallet'];
    })
  }

  increase(){
    console.log('incr', this.increament);
    console.log('wal', this.wallet);
    this.digitalCurrencyService.putWallet(Number(this.wallet) + Number(this.increament)).subscribe((user) => {
      console.log('success$' , user);
    })
  }
}
