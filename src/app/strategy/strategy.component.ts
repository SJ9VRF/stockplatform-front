import {Component, OnInit , ViewChild} from '@angular/core';
import{DragdropService} from '../services/dragdrop.service';
import{StrategyService} from '../services/strategy.service';
import{FlatTreeControl} from '@angular/cdk/tree';
import{MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {TREE_DATA} from '../shared/TreeNodes';
import{TreeNode} from '../shared/TreeNode';
import{ExampleFlatNode} from '../shared/ExampleFlatNode';
import{NgxDiagramComponent} from 'ngx-diagram';
import{DiagramNode} from '../shared/Diagramnode';
import {applyToPoint , inverse, identity} from 'transformation-matrix';
import{Strategy} from '../shared/strategy';
import { DiagramLink } from '../shared/DiagramLinks';
import { QuestionBase } from '../shared/question/question-base';
import{ MatDialog} from '@angular/material/dialog';
import { SaveStrategyComponent } from '../save-strategy/save-strategy.component';
import { LoadStrategyComponent } from '../load-strategy/load-strategy.component';
import { QuestionService } from '../services/question.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { CollectionNamesComponent } from '../collection-names/collection-names.component';
import { ignoreElements } from 'rxjs/operators';
import{ OutPut} from '../shared/output';
import{AuthService} from '../services/auth.service';
import{TranslocoService} from '@ngneat/transloco';

@Component({
  selector: 'app-strategy',
  templateUrl: './strategy.component.html',
  styleUrls: ['./strategy.component.scss'],
  providers: [QuestionService]
})
export class StrategyComponent implements OnInit {

  draggable = {
    data: "myDragData",
    effectAllowed: "all",
    disable: false,
    handle: false
  };
  changeQuestion : boolean = true;
  sourceId: string = '';
  draggingBlockName : string;
  draggingBlockId : string;
  ClickedBlockId : string;
  loadedStrategy : Strategy;
  nameToSaveLoadST: string;
  questions$: Observable<QuestionBase<any>[]>;
  lastMainNode : DiagramNode;
  lastMainNodeBuy : DiagramNode;
  myStrategy : Strategy;
  _matrix = identity();
  startPrice : number[] = [];
  endPrice : number[] = [];
  profit : number [] = [];
  sellNodes: DiagramNode[];
  sellLinks : DiagramNode[];
  buyNodes: DiagramNode[];
  buyLinks : DiagramLink[];
  collectionNames : string[]=['آرمان','اتکای'];
  runCollections : string[]= this.collectionNames;
  output : OutPut []= [];
  showOutPut : boolean = false;
  Signaltype : string = 'shop';
  runNodes: DiagramNode[];
  startDate : number;
  endDate : number;
  volume : number[];
  typesOfShoes: string[] = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];
  searchWord : string;
  wholeDesignedStrategies : string [] = [];
  wholePurchasedStrategies: string[] = [];
  wholeTestStrategies : string[] =[];
  designedStrategies : string [] = [];
  purchasedStrategies: string[] = [];
  testStrategies : string[] =[];

  @ViewChild('diagram') diagram: NgxDiagramComponent;


  constructor(private dragDropService : DragdropService , private strategyService : StrategyService,
    public dialog: MatDialog, private questionService : QuestionService,
    public router: Router, private authService : AuthService , private translocoService : TranslocoService) {
    this.dataSource.data = TREE_DATA;
   }
  //get strategies to show in list 
  ngOnInit() {
    this.authService.getProfileInformation().subscribe((user) => {
      this.wholePurchasedStrategies = user.purchased;
      this.purchasedStrategies = this.wholePurchasedStrategies;
    })
    this.strategyService.getStrategy().subscribe((strategies) => {
      for(let str of strategies){
        this.wholeDesignedStrategies.push(str.Name);
        this.designedStrategies = this.wholeDesignedStrategies;
        if(str.forSell){
          this.wholeTestStrategies.push(str.Name);
          this.testStrategies = this.wholeTestStrategies;
        }  
      }
    })
  }

  searchThis(){
    if(this.searchWord){
      this.testStrategies=  this.wholeTestStrategies
      .filter((str) => (str.includes(this.searchWord)));
      this.designedStrategies=  this.wholeDesignedStrategies
      .filter((str) => (str.includes(this.searchWord)));
      this.purchasedStrategies=  this.wholePurchasedStrategies
      .filter((str) => (str.includes(this.searchWord)));
    }
    else {
      this.testStrategies = this.wholeTestStrategies;
      this.designedStrategies = this.wholeDesignedStrategies;
      this.purchasedStrategies = this.wholePurchasedStrategies;
    }
  }
  //set the drop position
  onDrop(event){
      const recBound = this.diagram.divLayerRef.nativeElement.getBoundingClientRect() ;
      const relativeClientX = event.event.clientX - recBound.left;
      const relativeClientY = event.event.clientY - recBound.top;
      const creation = applyToPoint(inverse(this._matrix), { x: relativeClientX, y: relativeClientY });
      //call node create function 
      this.created(creation);
  };
  //set dragging block id and name
  mouseEnterBlock(blockName: string , blockId : string) {
    this.draggingBlockName = blockName;
    this.draggingBlockId = blockId;
  }
  //set dragging block id and name
  mouseLeaveBlock(){
    this.draggingBlockName = '';
    this.draggingBlockId = '';
  }
  //get stock names
  selectCollectionNames(){
    let collectionNamesDialog = this.dialog.open(CollectionNamesComponent, {width : '30vw'});
    collectionNamesDialog.afterClosed().subscribe((result) => {
      this.collectionNames = result;
      this.runCollections = this.collectionNames;
    })
  }
  saveStrategy(){
    let saveDialog = this.dialog.open(SaveStrategyComponent , {width: '20vw',
    data:{name : this.nameToSaveLoadST } });
    saveDialog.afterClosed().subscribe((result) => {
      if(result){
        this.strategyService.saveStrategy({Name : result ,Nodes : this.nodes , Links : this.links })
        .subscribe((strategy) => {
          this.myStrategy = strategy;
        });
      }
  });
    
  }
  openLoadDialog(){
    let loadDialog = this.dialog.open(LoadStrategyComponent , {width : '20vw' ,
     data:{name :this.nameToSaveLoadST }});
     loadDialog.afterClosed().subscribe((result) => {
      this.loadStrategy(result); })    
  }
  loadStrategy(name : string){
    console.log('strName', name);
    this.strategyService.loadStrategy(name).subscribe((strategy) =>{ 
      this.myStrategy = strategy;
      for(let node of strategy.Nodes){
        this.nodes.push(new DiagramNode(node));
      }
      
      this.links = strategy.Links;
      let lastNode;
      ///set blocks inputs
      for(let link of this.links){
        let targetId = link.target;
        let sourceId = link.source;
        let targetNode = this.nodes.find((node) => (node.id === targetId));
        let targetName = targetNode.name;
        if(targetName ==="Signal"){
          let sourceNode = this.nodes.find((node) => (node.id === sourceId));
          targetNode.value1 = sourceNode;
        }
        //set the last node(block) in node chain
        else if(targetName == "And" || targetName == "Cross" ||   targetName =="Or" || targetName =="Comparison"){
          console.log('tragetName', targetName);
          let sourceNode = this.nodes.find((node) => (node.id === sourceId));
          if(!targetNode.value1 ){
            targetNode.value1 = sourceNode;
          }
          else{
           targetNode.value2 =sourceNode;
            
              
              this.lastMainNode = targetNode;
          }
        }
        else{
          console.log('tarna', targetName);
        }
      }
      this.diagram.updateNodes(this.myStrategy.Nodes);
      for ( let node of this.myStrategy.Nodes){
        this.diagram.moveNodeTo(node.id , node.NodeX , node.NodeY);
      }
      this.diagram.updateLinks(this.myStrategy.Links);
      this.diagram.redraw();  
    });
  }

  //sell user strategy
  forSell(){
    this.strategyService.forSell(this.myStrategy._id, 20000)
    .subscribe((strategy) => console.log('forSellStrategy', strategy));
  }
  //change block properties when the property form is submitted
  setBlockProperty(payLoad : any){
    let node = this.nodes.find(node => (node.id === this.ClickedBlockId));
    for(let prop in payLoad){
      if(payLoad[prop] !==''){
        node[prop] = payLoad[prop];
      }
    }

  }
  //run strategy for each selected stock
  run(){
    console.log('run',this.runCollections);
    if(this.runCollections[0]){
      this.profit = [];
      this.startPrice = [];
      this.endPrice = [];
      console.log('runcollections[0]',this.runCollections[0]);
      this.runStrategy(false,this.runCollections.shift());
    }
    else {
      this.runCollections = this.collectionNames;
      this.showOutPut = true;
      this.strategyService.setStrategyOutPut(this.output);
      this.router.navigate(['strategyOutput']);
    }
    
  }


  runStrategy(forBuy: boolean, collectionName : string){
   
    let signalIndex = this.nodes.findIndex((node) => (node.name === "Signal"));
    
    if(forBuy){
        this.runNodes = this.nodes.slice(signalIndex+1 , this.nodes.length);
        this.lastMainNode = this.runNodes.find((node) => (node.name === "Signal")).value1;
    }
    else{
      this.runNodes = this.nodes.slice(0 , signalIndex+1);
      this.lastMainNode = this.runNodes.find((node) => (node.name === "Signal")).value1;
    }

    for(let node of this.runNodes){
      node.isReady = false;
      //call each block function : when each function has done its work call the last main node
      //last main node run if its inputs are ready
      if(node.name === "commonMath"){
        this.commonMath(node,this.Signaltype,collectionName);
      }
      else if(node.name === "Comparison"){
        this.comparison(node, this.Signaltype,collectionName);
      }
      else if(node.name === "StochRsi"){
        this.stochRSI(node, this.Signaltype,collectionName);
      }
      else if(node.name === "Ichimoku"){
        this.Ichimoku(node, this.Signaltype,collectionName);
      }
      else if(node.name==="OBV"){
        this.OBV(node,this.Signaltype,collectionName);
      }
      else if(node.name ==="MFI"){
        this.MFI(node,this.Signaltype,collectionName);
      }
      else if(node.name === "CCI" ){
        this.CCI(node, this.Signaltype,collectionName);
      }
      else if(node.name === "ADX" || node.name ==="Williams %R" || node.name ==="ATR" || node.name === "Stochastic"){
          this.ADX_ATR_Williams_Stochastic(node,this.Signaltype,collectionName);
      }
      else if(node.name ==="MA" || node.name ==="MACD" || node.name ==="PriceData"
      || node.name ==="RSI" || node.name == "BollingerBands"){
        //request for data,send node.timeframe and practical priceq
        this.MA_MACD_Bollinger(node,this.Signaltype,collectionName);
      }

    }
  }

  comparison(node : DiagramNode , SignalType,collectionName : string ){
    node.blockFunction();
    this.lastMainNodeResult(this.Signaltype,collectionName);
  }
  commonMath(node : DiagramNode, Signaltype,collectionName : string){
    this.strategyService.getPriceData(node.timeframe,node.practicalPrice,collectionName).subscribe((values) => {
      values = values.map((value) => value.price);
      node.values = values.slice(0,node.period);
      node.isReady = true;
      if(this.lastMainNode){
        this.lastMainNodeResult(Signaltype,collectionName);
      }
    });
  }





  Ichimoku(node : DiagramNode , Signaltype,collectionName:string){
    let score = 0;
    this.strategyService.getPriceData(node.timeframe,'Low',collectionName).subscribe((values) => {
      values = values.map((value) => value.price);
      node.low = values;
      //score variable tells that when this block get all inputs from database
      score++;
      if(score === 2){
        node.isReady = true;
        this.lastMainNodeResult(Signaltype,collectionName);
      }
    }),
    this.strategyService.getPriceData(node.timeframe,'High',collectionName).subscribe((values) => {
      values = values.map((value) => value.price);
      node.high = values;
      score++;
      if(score === 2){
        node.isReady = true;
        this.lastMainNodeResult(Signaltype,collectionName);
      }
    })  
  }
  stochRSI(node : DiagramNode, Signaltype,collectionName:string){
    this.strategyService.getPriceData(node.timeframe,'Close',collectionName).subscribe((values) => {
      values = values.map((value) => value.price);
      node.values = values;
      node.isReady = true;
      if(this.lastMainNode){
        this.lastMainNodeResult(Signaltype,collectionName);
      }
    });
  }
  OBV(node : DiagramNode, Signaltype,collectionName:string){
    //score variable tells that when this block get all inputs from database
    let score = 0;
    this.strategyService.getPriceData(node.timeframe,'Close',collectionName).subscribe((values) => {
      values = values.map((value) => value.price);
      node.close = values;
      score++;
      if(score === 2){
        node.isReady = true;
        this.lastMainNodeResult(Signaltype,collectionName);
      }
    }),
    this.strategyService.getPriceData(node.timeframe,'Volume',collectionName).subscribe((values) => {
      values = values.map((value) => value.price);
      node.volume = values;
      score++;
      if(score === 2){
        node.isReady = true;
        this.lastMainNodeResult(Signaltype,collectionName);
      }
    })
  }
MFI(node : DiagramNode, Signaltype,collectionName:string){
  //score variable tells that when this block get all inputs from database
  let score = 0;
        this.strategyService.getPriceData(node.timeframe,'Close',collectionName).subscribe((values) => {
          values = values.map((value) => value.price);
          node.close = values; 
          score++;
          if(score === 4){
            node.isReady = true;
            this.lastMainNodeResult(Signaltype,collectionName);
          }
        })
        this.strategyService.getPriceData(node.timeframe,'High',collectionName).subscribe((values) => {
          values = values.map((value) => value.price);
          node.high = values;
          score++;
          if(score === 4){
            node.isReady = true;
            this.lastMainNodeResult(Signaltype,collectionName);
          }
        })
        this.strategyService.getPriceData(node.timeframe,'Low',collectionName).subscribe((values) => {
          values = values.map((value) => value.price);
          node.low = values;
          score++;
          if(score === 4){
            node.isReady = true;
            this.lastMainNodeResult(Signaltype,collectionName);
          }
        })
        this.strategyService.getPriceData(node.timeframe,'Volume',collectionName).subscribe((values) => {
          values = values.map((value) => value.price);
          node.volume = values;
          score++;
          if(score === 4){
            node.isReady = true;
            this.lastMainNodeResult(Signaltype,collectionName);
          }
        })
}  
MA_MACD_Bollinger(node: DiagramNode, Signaltype,collectionName:string){
  node.isReady=false;
  this.strategyService.getPriceData(node.timeframe,node.practicalPrice,collectionName).subscribe((values) => {
    values = values.map((value) => value.price);
    console.log('values of',collectionName,':',values);
    node.values = values;
    node.isReady = true;
    if(this.lastMainNode){
      this.lastMainNodeResult(Signaltype,collectionName);
    }
  });
}

CCI(node : DiagramNode , Signaltype,collectionName:string){
  //score variable tells that when this block get all inputs from database
  let score = 0;
  this.strategyService.getPriceData(node.timeframe,'Close',collectionName).subscribe((values) => {
    values = values.map((value) => value.price);
    node.close = values;  
    score++;
    if(score === 4){
      node.isReady = true;
      this.lastMainNodeResult(Signaltype,collectionName);
    }
  })
  this.strategyService.getPriceData(node.timeframe,'High',collectionName).subscribe((values) => {
    values = values.map((value) => value.price);
    node.high = values; 
    score++;
    if(score === 4){
      node.isReady = true;
      this.lastMainNodeResult(Signaltype,collectionName);
    }
  })
  this.strategyService.getPriceData(node.timeframe,'Low',collectionName).subscribe((values) => {
    values = values.map((value) => value.price);
    node.low = values;
    score++;
    if(score ===4){
      node.isReady = true;
      this.lastMainNodeResult(Signaltype,collectionName);
    }
  })
  this.strategyService.getPriceData(node.timeframe,'Open',collectionName).subscribe((values) => {
    values = values.map((value) => value.price);
    node.open = values;
    score++;
    if(score === 4){
      node.isReady = true;
      this.lastMainNodeResult(Signaltype,collectionName);
    }
  })
}
ADX_ATR_Williams_Stochastic(node : DiagramNode , Signaltype,collectionName:string){
  //score variable tellss that when this block get all inputs from database
  let score = 0;
        this.strategyService.getPriceData(node.timeframe,'Close',collectionName).subscribe((values) => {
          values = values.map((value) => value.price);
          node.close = values; 
          score++;
          if(score === 3){
            node.isReady = true;
            this.lastMainNodeResult(Signaltype,collectionName);
          }
        })
        this.strategyService.getPriceData(node.timeframe,'High',collectionName).subscribe((values) => {
          values = values.map((value) => value.price);
          node.high = values;
          score++;
          if(score === 3){
            node.isReady = true;
            this.lastMainNodeResult(Signaltype,collectionName);
          }
        })
        this.strategyService.getPriceData(node.timeframe,'Low',collectionName).subscribe((values) => {
          values = values.map((value) => value.price);
          node.low = values;
          score++;
          if(score === 3){
            node.isReady = true;
            this.lastMainNodeResult(Signaltype,collectionName);
          }
        })
}




 lastMainNodeResult(Signaltype,collectionName : string){
    if(this.lastMainNode.name === "And" || this.lastMainNode.name === "Or"){
      if(this.lastMainNode.value1.setIsReady() && this.lastMainNode.value2.setIsReady()){
        let results  = this.lastMainNode.blockFunction();
        if(results.result){
          if(Signaltype === "shop"){
            console.log('pricestart',results.prices,'colname', collectionName);
            this.startPrice =results.prices;
            this.Signaltype="sell";
            this.runStrategy(true,collectionName);
          }
          else if(Signaltype === "sell"){
            console.log('priceend',results.price,'colname', collectionName);
            this.endPrice =results.prices;
            for(let i =0;i < this.startPrice.length; i++){
              if(this.endPrice[i]){
                if(this.endPrice[i] !== this.startPrice[i]){
                  this.profit[i] =  Number( (((this.endPrice[i] - this.startPrice[i])/this.startPrice[i]) * 100).toFixed(2));
                }
              }
            }
            this.strategyService.getDateVolume(collectionName).subscribe((result) => {
              this.output.push({name : collectionName , profit : this.profit, startDate : result.startDate, endDate : result.endDate,
              volume : result.volume});
              this.Signaltype = "shop";
              this.run();
            })
          }
        }
        else{
          this.run();
        }
      }
    }
    else if(this.lastMainNode.name === "Cross" ){
      if(this.lastMainNode.value1.isReady && this.lastMainNode.value2.isReady)
      {
        let results  = this.lastMainNode.blockFunction();
        if(Signaltype === "shop"){
          let j =0;
          for(let i=0; i < results.length; i++){
            if(results[i]){
              this.startPrice[j] = this.lastMainNode.value1.values[i];
              j++;
            }
          }
          this.Signaltype="sell"
          this.runStrategy(true,collectionName);
        }
        else if(Signaltype === "sell"){
          let j =0;
          for(let i=0; i < results.length; i++){
            if(results[i]){
              if(this.startPrice[j]){
                this.endPrice[j] = this.lastMainNode.value1.values[i];
                if(this.startPrice[j] !== this.endPrice[j]){
                  this.profit[j] =  Number( (((this.endPrice[j] - this.startPrice[j])/this.startPrice[j]) * 100).toFixed(2));
                  j++;
                }
              }
            }
          }
          console.log('priceend',this.endPrice,'colname', collectionName);
          this.strategyService.getDateVolume(collectionName).subscribe((result) => {
            this.output.push({name : collectionName , profit : this.profit, startDate : result.startDate, endDate : result.endDate, volume : result.volume});
            this.Signaltype = "shop";
            this.run();
          })
        }
        else{
          console.log('index is -1');
          this.run()
        }
      }  
    }
  }
  //******************************************************************************** */
  private _transformer = (node: TreeNode, level: number) => {
    let lang = this.translocoService.getActiveLang();
    let name = node.name;
    if(node.children){
      node.name = this.translocoService.translate(name ,{}, lang );
    }
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      id: node.id,
      level: level,
    };
  }

  treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
      this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);



  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

/******************************************************************* */
    selection : DiagramNode[]= [];
    nodes : DiagramNode [] = [];
    links : DiagramLink [] = [];

    //create link between nodes
    connected(connection) {
        //connection.stopPropagation();
        console.log('connectedevent', connection);
        if (connection.source.id !== connection.target.id) {

            if(this.links.length ===0){
                this.links = [{source: connection.source.id, target: connection.target.id}];
            }
            else{
                this.links.push({source: connection.source.id, target: connection.target.id});
            }
            

            this.diagram.updateLinks(this.links);
            this.diagram.redraw();
            if(connection.target.name ==="Signal"){
              connection.target.value1 = connection.source;
              this.lastMainNode = connection.source;
            }
            if(connection.target.name === "Cross" || connection.target.name ==="And" ||
            connection.target.name ==="Or" || connection.target.name ==="Comparison" ){
                    if(!connection.target.value1 ){
                      connection.target.value1 = connection.source;
                    }
                    else{
                      connection.target.value2 = connection.source;
                    }
            }
        }

    }
    //create node
    created(creation) {
       // creation.stopPropagation();
       const node = new DiagramNode({id: this.id() , name : this.draggingBlockName , 
        NodeX : creation.x , NodeY : creation.y});
        if(this.nodes.length ===0){
            this.nodes = [node];
        }
        else{
            this.nodes.push(node);
        }
        this.diagram.updateNodes(this.nodes);
        this.diagram.moveNodeTo(node.id, creation.x, creation.y);
        this.diagram.redraw();

    }
    //copy selected items in diagram
    copy(){
      for(let selectedNode of this.selection){
        let node = new DiagramNode({id : this.id() , name : selectedNode.name, NodeX : selectedNode.NodeX,
        NodeY : selectedNode.NodeY})
        this.nodes.push(node);
        this.diagram.updateNodes(this.nodes);
        this.diagram.moveNodeTo(node.id, selectedNode.NodeX + 5, selectedNode.NodeY + 5);
        this.diagram.redraw();
      }
    }
    //delete selected items in diagram
    delete(){
        this.deleteSelected();
    }
    selected(selection) {
        console.log('selected',selection);
        this.selection = selection;

    }

    deleteSelected() {

        this.nodes = this.nodes.filter(node => !this.selection.find(n => node.id === n.id));
        this.links = this.links.filter(link => !(this.selection.find(n => link.source === n.id || link.target === n.id)));
        for(let selected of this.selection){
          if(this.lastMainNode){
            if(selected.id === this.lastMainNode.id){
              this.lastMainNode = undefined;
            }
          }
          
        }
        this.diagram.updateNodes(this.nodes);
        this.diagram.redraw();
        this.selection = [];

    }

    deleteLinksBetweenSelected() {

        this.links = this.links.filter(link => !(this.selection.find(n => link.source === n.id) && this.selection.find(n => link.target === n.id)));

        this.diagram.updateLinks(this.links);
        this.diagram.redraw();

    }

    autoLayout() {
        this.diagram.autoLayout().then();
    }
    //generate id for nodes
    id(){
      return '_' + Math.random().toString(36).substr(2, 9);
    }

    onClick(event, node){
      this.ClickedBlockId = node.id;
      this.changeQuestion = !this.changeQuestion;
      this.questions$ = this.questionService.getQuestion(node);
      //create link between nodes
      if(event.offsetX < 0 || event.offsetX > 100){
        if(this.sourceId === ''){
          this.sourceId = node.id;
          this.diagram.startLink(event,node.id,'SOURCE');
          this.diagram.onWindowMouseMove(event);
          }
          else{
            this.diagram.endLink(event,node.id,'TARGET');
            this.sourceId = '';
          }
      }

   }
   _select_Mode : boolean = false;
   _node_mouse_down : boolean = false;
   _window_mouse_move : boolean = false;
   //start selection
   onAreaMousedblClick(event : any){
     this.diagram.startSelect(event);
     this._select_Mode = true;
   }
   //end selection
   onAreaMouseClick(event : any){
     if(this._select_Mode){
        
     this.diagram.endSelect(event);
     this._select_Mode = false;
     }
   }
   //for changing node position when is moved in diagram
   onNodeMouseDown(event : any, id : string){
     if(event.offsetX > 0 || event.offsetX < 100){
       this._node_mouse_down = true;
     }
  }
    //for changing node position when is moved in diagram
   onNodeMouseUp(event : any, id : string){
     if(this._window_mouse_move){
      const recBound = this.diagram.divLayerRef.nativeElement.getBoundingClientRect() ;
      const relativeClientX = event.clientX - recBound.left;
      const relativeClientY = event.clientY - recBound.top;
      const position = applyToPoint(inverse(this._matrix), { x: relativeClientX, y: relativeClientY });
      let node = this.nodes.find(node => (node.id === id));
      node.NodeX = position.x;
      node.NodeY = position.y;
      this._node_mouse_down = false;
      this._window_mouse_move = false;
     }
     else{
       this._node_mouse_down = false;
     }
   }
     //for changing node position when is moved in diagram
   onWindowMouseMove(event : any){
     if(this._node_mouse_down){
       this._window_mouse_move = true;
     }
   }
 
}
