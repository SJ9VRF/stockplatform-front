import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {User} from '../shared/user';
import {AuthService} from '../services/auth.service';
import{TranslocoService} from '@ngneat/transloco';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  @ViewChild('sForm') signUpFormDirective;

  user : {email : string, password: string};
  signUpForm : FormGroup;

  formErrors = {
    'email' : '',
    'password': ''
  };
  validationMessages = {
 
    'email' : { 
      'required': 'آدرس ایمیل الزامی است',
      'email' : 'فرمت ایمیل نادرست است'
    },
    'password' : {
      'required' : 'رمز عبور الزامی است'
    }
  }
  constructor(public dialogRef : MatDialogRef<SignupComponent>, private translocoService : TranslocoService,
    private fb : FormBuilder, private authService : AuthService) { }

  ngOnInit() {
    this.createForm();

  }

  createForm(){
    this.signUpForm = this.fb.group({
      email : ['',[ Validators.required , Validators.email]],
      password : ['' , [Validators.required]]
    });   
    this.signUpForm.valueChanges.subscribe((data) => this.onValueChanged(data));
   

  }
  onValueChanged(data : any){
    if(!this.signUpForm){return ;}
    const form = this.signUpForm;
    //clear previous err message
    for(const field in this.formErrors){
      if(this.formErrors.hasOwnProperty(field)){
        this.formErrors[field] = '';
        const control = form.get(field);
        //re check for erros
        if(control && control.dirty && !control.valid){
          const messages = this.validationMessages[field];
          console.log('control errors : ' ,control.errors);
          for(const key in control.errors){
            if(control.errors.hasOwnProperty(key)){
              console.log('key =' , key);
              console.log('message[key]', messages);
              this.formErrors[field] += this.translocoService.translate(`formErrors.${field}.${key}`) + '\t';
            }
          }
        }
      }
    }
  }
 
  onSubmitSignUpForm(){
    console.log('user first' , this.user);
    this.user = this.signUpForm.value;
    console.log('user second' , this.user);
    this.authService.signUp(this.user)
    .subscribe((result) => {
        console.log('result signup :' , result);
        this.dialogRef.close('SignUp Successful');
    }, (error) => {
      console.log('result signup :' ,error);
    });
}

}
