import { Component, OnInit } from '@angular/core';
import{ProfileManagmentService} from '../services/profile-managment.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  user = {password : ''}
  constructor(private profileManagmentService : ProfileManagmentService) { }

  ngOnInit() {
  }

  onSubmit(){
    this.profileManagmentService.changePassword(this.user.password).subscribe((user) => {
      console.log('user', user);
    })
  }
}
