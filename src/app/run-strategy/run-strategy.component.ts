//this component is used in testpackage page for package test 
import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import{Strategy} from '../shared/strategy';
import { DiagramLink } from '../shared/DiagramLinks';
import{StrategyService} from '../services/strategy.service';
import{DiagramNode} from '../shared/Diagramnode';
import { OutPut } from '../shared/output';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { createChart } from 'lightweight-charts';

@Component({
  selector: 'app-run-strategy',
  templateUrl: './run-strategy.component.html',
  styleUrls: ['./run-strategy.component.scss']
})
export class RunStrategyComponent implements OnInit {

  @Input() strName : string;
  strategy : Strategy;
  nodes : DiagramNode[]=[];
  links : DiagramLink[]=[];
  signalType : string = "shop";
  runCollections : string[];
  showOutPut : boolean = false;
  output : OutPut []= [];
  runNodes : DiagramNode[];
  @Input() collectionName : string ;
  @Input() startDate : number;
  @Input() endDate : number;
  lastMainNode : DiagramNode;
  startPrice : number[]=[];
  endPrice : number[]=[];
  profit : number[]=[];
  runCollection : string;

  constructor(private strategyService : StrategyService, public router: Router) { }

  ngOnInit(): void {
    console.log('init run str :', this.collectionName);
    this.runCollection = this.collectionName;
    this.loadStrategy();
  }
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      let chng = changes[propName];
      let cur  = chng.currentValue;
      let prev = chng.previousValue;
      console.log('prev',prev);
      console.log('curr',cur);
      if(propName === 'collectionName' && prev){
        console.log('changecolName',this.collectionName);
        this.runCollection = this.collectionName;
        this.run();
      }
      if(propName === 'startDate' && prev){
        console.log('startDate',this.startDate);
        this.run();
      }
      if(propName === 'endDate' && prev){
        console.log('endDate',this.endDate);
        this.run();
      }
    }
  }
  loadStrategy(){
    this.strategyService.loadStrategy(this.strName).subscribe((strategy) =>{ 
      this.strategy = strategy;
      for(let node of strategy.Nodes){
        this.nodes.push(new DiagramNode(node));
      }
      this.links = strategy.Links;
      let lastNode;
      //for run
      for(let link of this.links){
        let targetId = link.target;
        let sourceId = link.source;
        let targetNode = this.nodes.find((node) => (node.id === targetId));
        let targetName = targetNode.name;
        if(targetName ==="Signal"){
          let sourceNode = this.nodes.find((node) => (node.id === sourceId));
          targetNode.value1 = sourceNode;
        }
        else if(targetName == "And" || targetName == "Cross" ||   targetName =="Or" || targetName =="Comparison"){
          console.log('tragetName', targetName);
          let sourceNode = this.nodes.find((node) => (node.id === sourceId));
          if(!targetNode.value1 ){
            targetNode.value1 = sourceNode;
          }
          else{
            targetNode.value2 =sourceNode;       
            //this.lastMainNode = targetNode;
            }
        }
        else{
          console.log('tarna', targetName);
        }
      } 
      this.run();
    });
  }
  run(){
    if(this.runCollection){
      this.profit = [];
      this.startPrice = [];
      this.endPrice = [];
      console.log('runcollections[0]',this.collectionName);
      this.runStrategy(false,this.runCollection);
      this.showOutPut= false;
    }
    else {
      this.runCollection = this.collectionName;
      console.log('runcollections[0]',this.collectionName);
      this.showOutPut = true;
      this.strategyService.setStrategyOutPut(this.output);
    }
    
  }
  runStrategy(forBuy: boolean, collectionName : string){
    this.runCollection = '';
    let signalIndex = this.nodes.findIndex((node) => (node.name === "Signal"));
    
    if(forBuy){
        this.runNodes = this.nodes.slice(signalIndex+1 , this.nodes.length);
        this.lastMainNode = this.runNodes.find((node) => (node.name === "Signal")).value1;
    }
    else{
      this.runNodes = this.nodes.slice(0 , signalIndex+1);
      this.lastMainNode = this.runNodes.find((node) => (node.name === "Signal")).value1;
    }

    for(let node of this.runNodes){
      node.isReady = false;
      if(node.name === "commonMath"){
        this.commonMath(node,this.signalType,collectionName);
      }
      else if(node.name === "Comparison"){
        this.comparison(node, this.signalType,collectionName);
      }
      else if(node.name === "StochRsi"){
        this.stochRSI(node, this.signalType,collectionName);
      }
      else if(node.name === "Ichimoku"){
        this.Ichimoku(node, this.signalType,collectionName);
      }
      else if(node.name==="OBV"){
        this.OBV(node,this.signalType,collectionName);
      }
      else if(node.name ==="MFI"){
        this.MFI(node,this.signalType,collectionName);
      }
      else if(node.name === "CCI" ){
        this.CCI(node, this.signalType,collectionName);
      }
      else if(node.name === "ADX" || node.name ==="Williams %R" || node.name ==="ATR" || node.name === "Stochastic"){
          this.ADX_ATR_Williams_Stochastic(node,this.signalType,collectionName);
      }
      else if(node.name ==="MA" || node.name ==="MACD" || node.name ==="PriceData"
      || node.name ==="RSI" || node.name == "BollingerBands"){
        //request for data,send node.timeframe and practical priceq
        this.MA_MACD_Bollinger(node,this.signalType,collectionName);
      }
      else if(node.name === "Cross"){
        if(node.value1.isReady && node.value2.isReady ){
          node.isReady = true;
        }
      }
    }
  }
  comparison(node : DiagramNode , SignalType,collectionName : string ){
    node.blockFunction();
    this.lastMainNodeResult(this.signalType,collectionName);
  }
  commonMath(node : DiagramNode, Signaltype,collectionName : string){
    this.strategyService.getPriceData(node.timeframe,node.practicalPrice,
      collectionName, this.startDate, this.endDate).subscribe((values) => {
      values = values.map((value) => value.price);
      node.values = values.slice(0,node.period);
      node.isReady = true;
      if(this.lastMainNode){
        this.lastMainNodeResult(Signaltype,collectionName);
      }
    });
  }

  Ichimoku(node : DiagramNode , Signaltype,collectionName:string){
    let score = 0;
    this.strategyService.getPriceData(node.timeframe,'Low',collectionName, 
    this.startDate, this.endDate).subscribe((values) => {
      values = values.map((value) => value.price);
      node.low = values;
      score++;
      if(score === 2){
        node.isReady = true;
        this.lastMainNodeResult(Signaltype,collectionName);
      }
    }),
    this.strategyService.getPriceData(node.timeframe,'High',collectionName
    , this.startDate, this.endDate).subscribe((values) => {
      values = values.map((value) => value.price);
      node.high = values;
      score++;
      if(score === 2){
        node.isReady = true;
        this.lastMainNodeResult(Signaltype,collectionName);
      }
    })  
  }
  stochRSI(node : DiagramNode, Signaltype,collectionName:string){
    this.strategyService.getPriceData(node.timeframe,'Close',collectionName
    , this.startDate, this.endDate).subscribe((values) => {
      values = values.map((value) => value.price);
      node.values = values;
      node.isReady = true;
      if(this.lastMainNode){
        this.lastMainNodeResult(Signaltype,collectionName);
      }
    });
  }
  OBV(node : DiagramNode, Signaltype,collectionName:string){
    let score = 0;
    this.strategyService.getPriceData(node.timeframe,'Close',collectionName
    , this.startDate, this.endDate).subscribe((values) => {
      values = values.map((value) => value.price);
      node.close = values;
      score++;
      if(score === 2){
        node.isReady = true;
        this.lastMainNodeResult(Signaltype,collectionName);
      }
    }),
    this.strategyService.getPriceData(node.timeframe,'Volume',collectionName
    , this.startDate, this.endDate).subscribe((values) => {
      values = values.map((value) => value.price);
      node.volume = values;
      score++;
      if(score === 2){
        node.isReady = true;
        this.lastMainNodeResult(Signaltype,collectionName);
      }
    })
  }
MFI(node : DiagramNode, Signaltype,collectionName:string){
  let score = 0;
        this.strategyService.getPriceData(node.timeframe,'Close',collectionName
        , this.startDate, this.endDate).subscribe((values) => {
          values = values.map((value) => value.price);
          node.close = values; 
          score++;
          if(score === 4){
            node.isReady = true;
            this.lastMainNodeResult(Signaltype,collectionName);
          }
        })
        this.strategyService.getPriceData(node.timeframe,'High',collectionName
        , this.startDate, this.endDate).subscribe((values) => {
          values = values.map((value) => value.price);
          node.high = values;
          score++;
          if(score === 4){
            node.isReady = true;
            this.lastMainNodeResult(Signaltype,collectionName);
          }
        })
        this.strategyService.getPriceData(node.timeframe,'Low',collectionName
        , this.startDate, this.endDate).subscribe((values) => {
          values = values.map((value) => value.price);
          node.low = values;
          score++;
          if(score === 4){
            node.isReady = true;
            this.lastMainNodeResult(Signaltype,collectionName);
          }
        })
        this.strategyService.getPriceData(node.timeframe,'Volume',collectionName
        , this.startDate, this.endDate).subscribe((values) => {
          values = values.map((value) => value.price);
          node.volume = values;
          score++;
          if(score === 4){
            node.isReady = true;
            this.lastMainNodeResult(Signaltype,collectionName);
          }
        })
}  
MA_MACD_Bollinger(node: DiagramNode, Signaltype,collectionName:string){
  node.isReady=false;
  this.strategyService.getPriceData(node.timeframe,node.practicalPrice,collectionName
    , this.startDate, this.endDate).subscribe((values) => {
    values = values.map((value) => value.price);
    console.log('values of',collectionName,':',values);
    node.values = values;
    node.isReady = true;
    if(this.lastMainNode){
      this.lastMainNodeResult(Signaltype,collectionName);
    }
  });
}

CCI(node : DiagramNode , Signaltype,collectionName:string){
  let score = 0;
  this.strategyService.getPriceData(node.timeframe,'Close',collectionName
  , this.startDate, this.endDate).subscribe((values) => {
    values = values.map((value) => value.price);
    node.close = values;  
    score++;
    if(score === 4){
      node.isReady = true;
      this.lastMainNodeResult(Signaltype,collectionName);
    }
  })
  this.strategyService.getPriceData(node.timeframe,'High',collectionName
  , this.startDate, this.endDate).subscribe((values) => {
    values = values.map((value) => value.price);
    node.high = values; 
    score++;
    if(score === 4){
      node.isReady = true;
      this.lastMainNodeResult(Signaltype,collectionName);
    }
  })
  this.strategyService.getPriceData(node.timeframe,'Low',collectionName
  , this.startDate, this.endDate).subscribe((values) => {
    values = values.map((value) => value.price);
    node.low = values;
    score++;
    if(score ===4){
      node.isReady = true;
      this.lastMainNodeResult(Signaltype,collectionName);
    }
  })
  this.strategyService.getPriceData(node.timeframe,'Open',collectionName
  , this.startDate, this.endDate).subscribe((values) => {
    values = values.map((value) => value.price);
    node.open = values;
    score++;
    if(score === 4){
      node.isReady = true;
      this.lastMainNodeResult(Signaltype,collectionName);
    }
  })
}
ADX_ATR_Williams_Stochastic(node : DiagramNode , Signaltype,collectionName:string){
  let score = 0;
        this.strategyService.getPriceData(node.timeframe,'Close',collectionName
        , this.startDate, this.endDate).subscribe((values) => {
          values = values.map((value) => value.price);
          node.close = values; 
          score++;
          if(score === 3){
            node.isReady = true;
            this.lastMainNodeResult(Signaltype,collectionName);
          }
        })
        this.strategyService.getPriceData(node.timeframe,'High',collectionName
        , this.startDate, this.endDate).subscribe((values) => {
          values = values.map((value) => value.price);
          node.high = values;
          score++;
          if(score === 3){
            node.isReady = true;
            this.lastMainNodeResult(Signaltype,collectionName);
          }
        })
        this.strategyService.getPriceData(node.timeframe,'Low',collectionName
        , this.startDate, this.endDate).subscribe((values) => {
          values = values.map((value) => value.price);
          node.low = values;
          score++;
          if(score === 3){
            node.isReady = true;
            this.lastMainNodeResult(Signaltype,collectionName);
          }
        })
}

lastMainNodeResult(Signaltype,collectionName : string){
  if(this.lastMainNode.name === "And" || this.lastMainNode.name === "Or"){
    if(this.lastMainNode.value1.setIsReady() && this.lastMainNode.value2.setIsReady()){
      let results  = this.lastMainNode.blockFunction();
      if(results.result){
        if(Signaltype === "shop"){
          console.log('pricestart',results.prices,'colname', collectionName);
          this.startPrice =results.prices;
          this.signalType="sell";
          this.runStrategy(true,collectionName);
        }
        else if(Signaltype === "sell"){
          console.log('priceend',results.price,'colname', collectionName);
          this.endPrice =results.prices;
          for(let i =0;i < this.startPrice.length; i++){
            if(this.endPrice[i]){
              if(this.endPrice[i] !== this.startPrice[i]){
                this.profit[i] =  Number( (((this.endPrice[i] - this.startPrice[i])/this.startPrice[i]) * 100).toFixed(2));
              }
            }
          }
          this.strategyService.getDateVolume(collectionName,this.startDate,this.endDate).subscribe((result) => {
            this.output.push({name : collectionName , profit : this.profit, startDate : result.startDate, endDate : result.endDate,
            volume : result.volume});
            this.signalType = "shop";
            this.run();
          })
        }
      }
      else{
        this.run();
      }
    }
  }
  else if(this.lastMainNode.name === "Cross" ){
    if(this.lastMainNode.value1.isReady && this.lastMainNode.value2.isReady)
    {
      let results  = this.lastMainNode.blockFunction();
      if(Signaltype === "shop"){
        let j =0;
        for(let i=0; i < results.length; i++){
          if(results[i]){
            this.startPrice[j] = this.lastMainNode.value1.values[i];
            j++;
          }
        }
        this.signalType="sell"
        this.runStrategy(true,collectionName);
      }
      else if(Signaltype === "sell"){
        let j =0;
        for(let i=0; i < results.length; i++){
          if(results[i]){
            if(this.startPrice[j]){
              this.endPrice[j] = this.lastMainNode.value1.values[i];
              if(this.startPrice[j] !== this.endPrice[j]){
                this.profit[j] =  Number( (((this.endPrice[j] - this.startPrice[j])/this.startPrice[j]) * 100).toFixed(2));
                j++;
              }
            }
          }
        }
        console.log('priceend',this.endPrice,'colname', collectionName);
        this.strategyService.getDateVolume(collectionName,this.startDate,this.endDate).subscribe((result) => {
          this.output.push({name : collectionName , profit : this.profit, startDate : result.startDate, endDate : result.endDate,
          volume : result.volume});
          this.signalType = "shop";
          this.run();
        })
      }
      else{
        console.log('index is -1');
        this.run()
      }
    }  
  }
} 
}
