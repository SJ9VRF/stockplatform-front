import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunStrategyComponent } from './run-strategy.component';

describe('RunStrategyComponent', () => {
  let component: RunStrategyComponent;
  let fixture: ComponentFixture<RunStrategyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunStrategyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunStrategyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
