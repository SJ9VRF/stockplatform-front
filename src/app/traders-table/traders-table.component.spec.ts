import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradersTableComponent } from './traders-table.component';

describe('TradersTableComponent', () => {
  let component: TradersTableComponent;
  let fixture: ComponentFixture<TradersTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradersTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradersTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
