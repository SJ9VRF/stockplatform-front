//show digital currency sellers and buyers
import {Component, OnInit, ViewChild, Input, SimpleChange, SimpleChanges} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import{DigitalCurrencyService} from '../services/digital-currency.service';

export interface TraderData {
  value: number;
  unitPrice: number;
  totalPrice: number;
}

@Component({
  selector: 'app-traders-table',
  templateUrl: './traders-table.component.html',
  styleUrls: ['./traders-table.component.scss']
})
export class TradersTableComponent implements OnInit {

  displayedColumns: string[] = ['value', 'unitPrice', 'totalPrice'];
  dataSource: MatTableDataSource<TraderData>;

  @Input() currencyName : string;
  @Input() refresh : boolean ;  
  @Input() type : string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor( private digitalCurrencyService : DigitalCurrencyService) { }

  ngOnChanges(changes : SimpleChanges){
    for (let propName in changes) {
      let chng = changes[propName];
      let cur  = chng.currentValue;
      let prev = chng.previousValue;
      console.log('cur',cur);
      if(propName === 'currencyName' ){
        this.currencyName = cur;
        this.getData();
      }
      if(propName === "refresh"){
        this.getData();
      }
    }
  }
  ngOnInit() {
    this.getData();
    
  }

  getData(){
    console.log('currencyname', this.currencyName);
    if(this.type === "sellers"){
      this.digitalCurrencyService.getSellers(this.currencyName).subscribe((sellers) => {
        let sells = sellers[0];
        for(let order of sells.orders){
          console.log('order', order);
          order.totalPrice = order.unitPrice * order.value;
        }
        console.log('sellers',sells.orders);
        this.dataSource = new MatTableDataSource(sells.orders);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
    }
    else{
      this.digitalCurrencyService.getShopper(this.currencyName).subscribe((shoppers) => {
        let shops = shoppers[0];
        for(let order of shops.orders){
          console.log('order', order);
          order.totalPrice = order.unitPrice * order.value;
        }
        console.log('shoppers',shops.orders);
        this.dataSource = new MatTableDataSource(shops.orders);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })

    }
  }
}
