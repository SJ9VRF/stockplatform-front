import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdforumComponent } from './adforum.component';

describe('AdforumComponent', () => {
  let component: AdforumComponent;
  let fixture: ComponentFixture<AdforumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdforumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdforumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
