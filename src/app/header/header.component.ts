import { Component, OnInit , OnDestroy, OnChanges, HostListener} from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import{SignupComponent} from 'src/app/signup/signup.component';
import { LoginComponent } from '../login/login.component';
import{AuthService} from '../services/auth.service';
import { Subscription } from 'rxjs';
import{ProfileManagmentService} from '../services/profile-managment.service';
import { baseurl } from '../shared/baseurl';
import{ Router, NavigationEnd } from '@angular/router';
import{ TranslocoService } from "@ngneat/transloco";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit , OnChanges{

  profileVisibility : boolean = false;
  username : string;
  userNameSubscription : Subscription;
  visibilitySubscription : Subscription;
  imageSubscription : Subscription;
  mySubscription : Subscription;
  imageAddress : string = baseurl + 'images/static/' + 'profileDefualt.png';
  logoImageAddress : string = baseurl + 'images/static/Logo1.png';
  address : string = baseurl + 'images/static/';
  avatar : number;
  name : string;
  flag : number;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.avatar  = window.innerWidth * 0.04;
        this.flag = window.innerWidth * 0.015;
        //this.resize();
  }
  

  constructor(public dialog : MatDialog, private authService : AuthService,
    private profileManagmentService : ProfileManagmentService, private router : Router,
    private translocoService : TranslocoService) {
            
      
     }

  ngOnInit() {
    

    this.userNameSubscription = this.authService.getUsername().subscribe((name) => {
      console.log('name from loadcredentials' + name);
      this.username = name;
    });
    this.visibilitySubscription = this.authService.getVisibility()
    .subscribe((visibility) => {
        this.profileVisibility = visibility;
        this.imageSubscription=this.profileManagmentService.getProfileImage().subscribe((image) => {
          if(image.image){
             this.imageAddress = baseurl + 'images/profile/' + image.image;
          }
          else{
            this.imageAddress = baseurl + 'images/static' + 'profileDefualt';
          }
      });
      });
    this.authService.loadUserCredentials();
    if(this.authService.isLoggedIn()){
      this.profileVisibility = true;
    }

    this.profileManagmentService.getUserProfile().subscribe((user) => {
      if(user.firstName && user.lastName){
        this.name = user.firstname+ '' + user.lastname;
      }
      else if(user.username){
        this.name = user.username;
      }
      else{
        this.name="new user"
      }
    });
    this.avatar = window.innerWidth * 0.04;
    this.flag = window.innerWidth * 0.015;
    let lang = this.translocoService.getActiveLang();
    if(lang === "en"){
      this.logoImageAddress =  baseurl + 'images/static/hd-en-logo.jpeg';
    }
    else {
      this.logoImageAddress =  baseurl + 'images/static/hd-fa-logo.jpeg';
    }
    
  }
  ngOnDestroy(){
    this.userNameSubscription.unsubscribe();
    this.visibilitySubscription.unsubscribe();
    this.imageSubscription.unsubscribe();

  }
  openSignUpDialog(){
    this.dialog.open(SignupComponent, { width : '30vw'});
  }
  openLogInDialog(){
    let loginDialog = this.dialog.open(LoginComponent , {width : '30vw' , data : {username : this.username} });
    loginDialog.afterClosed().subscribe((result) => {
      if(result){
        console.log('result', result);
        this.imageSubscription=this.profileManagmentService.getProfileImage().subscribe((image) => {
          console.log('image.image' , image.image);
          this.imageAddress = baseurl + 'images/profile/' + image.image;
        });
    
      }})
  }
  ngOnChanges(){
    console.log('leave');
  }
  logOut(){
    this.username = undefined;
    this.profileVisibility = false;
    this.imageAddress = undefined;
    this.authService.logOut();
    this.router.navigate(['home']);
  }
  changeLang(lang : string){
    this.translocoService.setActiveLang(lang);
    if(lang === "en"){
      this.logoImageAddress =  baseurl + 'images/static/hd-en-logo.jpeg';
    }
    else {
      this.logoImageAddress =  baseurl + 'images/static/hd-fa-logo.jpeg';
    }
  }
}
