//this component work for admin only and get users that sent authentication information to data base
import { Component, OnInit ,ViewChild} from '@angular/core';
import{ProfileManagmentService} from '../services/profile-managment.service'
import{User} from '../shared/user';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import{ TranslocoService } from '@ngneat/transloco';

interface tableData{
  firstName : string,
  lastName : string,
  status : string,
  id : string,
  //creationDate : string
}
@Component({
  selector: 'app-identity-confirmation',
  templateUrl: './identity-confirmation.component.html',
  styleUrls: ['./identity-confirmation.component.scss']
})
export class IdentityConfirmationComponent implements OnInit {

  users : User[];
  displayedColumns: string[] = ['firstName', 'lastName', 'status'];
  dataSource: MatTableDataSource<tableData>;
  tableData : tableData[]= [];
  @ViewChild(MatSort) sort: MatSort;
  
  constructor(private profilleManagmentService : ProfileManagmentService) { }

  ngOnInit(): void {
    this.profilleManagmentService.getUsersByAdmin().subscribe((users) => {
      users = users.filter((user) => user.status)
      
      for(let user of users){
        this.tableData.push({firstName : user.firstName, lastName : user.lastName, status : user.status, id : user._id});
      }
      this.dataSource = new MatTableDataSource(this.tableData);
      this.dataSource.sort = this.sort;
      console.log('users', users);
    })
  }

}
