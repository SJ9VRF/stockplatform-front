//test
import { Component, OnInit, HostListener, SimpleChanges } from '@angular/core';
import{Strategy} from '../shared/strategy';
import{StrategyService} from '../services/strategy.service';
import{ActivatedRoute} from '@angular/router';
import{AuthService} from '../services/auth.service';
import {ApiService} from '../services/api.service';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { createChart } from 'lightweight-charts';
import { MAT_RIPPLE_GLOBAL_OPTIONS } from '@angular/material/core';
import{TranslocoService} from  '@ngneat/transloco';


const PersianDate = require('persian-date');



@Component({
  selector: 'app-ai-engine',
  templateUrl: './ai-engine.component.html',
  styleUrls: ['./ai-engine.component.scss'],
  //providers: [TradingViewAiComponent]
})
export class AiEngineComponent implements OnInit {

  wholeStrategies : Strategy[];
  strategies : Strategy[];
  wholeStocks : string[];
  stock : string;
  stocks : string[];
  wholeCollections : any[];
  collection : any;
  selectedPackage : string = '';
  selectedStock : string;
  startDate : string ='';
  endDate : string = '';
  startDay : number;
  endDay : number;
  run: boolean = false;
  real_prediction_values;
  //chart variables
  legend: boolean =false;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'time';
  yAxisLabel: string = 'price';
  timeline: boolean = true;
  view: any[] ;
  multi : any[];
  wCoefficient : number = 0.4;
  hCoefficient : number =0.4;
  config = {
    drops : "down"
  }
  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };
  chart : any;

/*
  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.view[1] = window.innerHeight * this.hCoefficient;
        this.view[0] = window.innerWidth * this.wCoefficient;
        console.log(this.view[0], this.view[1]);
  }
*/
  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.view[1] = window.innerWidth * this.hCoefficient;
        this.view[0] = window.innerWidth * this.wCoefficient;
        this.chart.resize(this.view[0],this.view[1]);
        this.chart.applyOptions({
          layout: {
              fontSize: 0.01 *window.innerWidth ,
          },
      });        
  }

  constructor(private strategyService : StrategyService, private authService : AuthService,
    private apiService : ApiService, private translocoService : TranslocoService) {
    //private apiService : ApiService, private tradingviewai : TradingViewAiComponent ) {
     }

  runStrategy(){
    //convert date to timestamp
    let startD = this.startDate.split('-');
    let endD= this.endDate.split('-');
    console.log('sday',startD)
    console.log('eday', endD);
    this.startDay = new PersianDate([Number(startD[0]),Number(startD[1]),Number(startD[2])]).format("X");
    this.startDay = this.startDay * 1000;
    this.endDay = new PersianDate([Number(endD[0]),Number(endD[1]),Number(endD[2])]).format("X");
    this.endDay = this.endDay * 1000;
    console.log('startDay', this.startDay);
    console.log('endDay', this.endDay);
    console.log('selectedstock is: ', this.selectedStock);
    //this.tradingviewai.ngOnInit();

    this.apiService.predictor(this.selectedStock ,this.startDay,this.endDay).subscribe((results) => {
      var lineSeries = this.chart.addLineSeries(       { color: "red",
      lineStyle: 0,
      lineWidth: 2,
      crosshairMarkerVisible: true,
      crosshairMarkerRadius: 6,
      lineType: 2
     })

        for(let result of results[0]['prediction_value']) {
          console.log("the result1 is" , result);
          lineSeries.update({time : result.time , value : result.price})
        }
      var lineSeries2 = this.chart.addLineSeries(       { color: 'blue',
      lineStyle: 0,
      lineWidth: 2,
      crosshairMarkerVisible: true,
      crosshairMarkerRadius: 6,
      lineType: 2
     })

        for(let result of results[0]['real_value']) {
          console.log("the result2 is" , result);

          lineSeries2.update({time : result.time , value : result.price})
        }


    /*
      console.log('this is theeeeeeee result', results);
       let pair = results[0].time_stamp_value;
       let keys = Object.keys(pair);
       let values : number[] = Object.values(pair);
       let multi = [{"name" : "kachad", "series": [{"name" : "1", "value" : 10},{"name" : "2", "value" : 5},
       {"name" : "3", "value" : 20}]}];
       //set chart data
       for(let i=0; i< keys.length; i++){
         multi[0].series.push({"name" : keys[i], "value" : values[i]})
       }
       //assign chart data
        Object.assign(this , {multi});
        */
    })
   // this.run = true;
   
  }
  
  ngOnInit(){
    let width = this.wCoefficient * window.innerWidth;
    let height = this.hCoefficient * window.innerHeight;
    this.view = [width, height]
    //load jwt
    this.authService.loadUserCredentials();
    this.strategyService.getForSellStrategies().subscribe((strategy) => {
      this.wholeStrategies = strategy;
      this.strategies = this.wholeStrategies;
    });
    this.strategyService.getCollectionNames().subscribe((CollectionNames) =>{
      this.wholeStocks = CollectionNames;
      this.stocks = this.translocoService.translate(this.wholeStocks);
      console.log("this is whole stockssssss", this.stocks);
    });
    this.strategyService.getCollections().subscribe((Collections) =>{
      this.wholeCollections = Collections;
      //this.collection = this.wholeCollections;
    });

    
    this.view[0] = window.innerWidth * this.wCoefficient;
    this.view[1] = window.innerHeight * this.hCoefficient;

    let myChart = document.getElementById('chart');
    this.authService.loadUserCredentials();
    this.chart = createChart(myChart, { width: this.view[0], height: this.view[1] });
    this.chart.applyOptions({
      layout: {
          fontSize: 0.01 *window.innerWidth ,
      },
  });
}


  searchPackage(searchWord : string){
    console.log('keuup', searchWord)
    if(searchWord){
      this.strategies=this.wholeStrategies.filter((str) => str.Name.includes(searchWord));
    }
    else{
      this.strategies = this.wholeStrategies;
    }
  }
  searchStock(searchWord : string){
    if(searchWord){
      this.stocks = this.wholeStocks.filter((stock) => stock.includes(searchWord));
      this.collection = this.wholeCollections.filter((col) => col.includes(searchWord)._id);
      console.log("this is the collection     ", this.collection) 
    }
    else{
      this.stocks = this.wholeStocks;
    }
  }

  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }


 
ngOnChanges(changes: SimpleChanges) {
  for (let propName in changes) {
    let chng = changes[propName];
    let cur  = chng.currentValue;
    let prev = chng.previousValue;
    console.log('prev',prev);
    console.log('curr',cur);
   // if(this.selectedStock){
      console.log('else part occured',this.selectedStock);
      let myChart = document.getElementById('chart');
      myChart.removeChild(myChart.firstChild);
      const chart = createChart(myChart, { width: 600, height: 500 });

      this.apiService.predictor(this.selectedStock ,this.startDay,this.endDay).subscribe((results) => {
        var lineSeries = this.chart.addLineSeries(   
);
        for(let result of results[0]['prediction_value']) {
          console.log("the result iiiiiiiiiiiiiiiiis" , result);
          lineSeries.update({time : result.time , value : result.price})
        }
       var lineSeries2 = this.chart.addLineSeries(
);

        //for(let result of results[0]['prediction_value']) {
          //lineSeries.update({time : result.time , value : result.price})
        //}
        for(let result of results[0]['real_value']) {
          lineSeries2.update({time : result.time , value : result.price})
        }
    })
   // }
  }
}

}
