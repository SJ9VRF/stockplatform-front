import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AiEngineComponent } from './ai-engine.component';

describe('AiEngineComponent', () => {
  let component: AiEngineComponent;
  let fixture: ComponentFixture<AiEngineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AiEngineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AiEngineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
