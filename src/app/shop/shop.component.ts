import { Component, OnInit, HostListener } from '@angular/core';
import{ StrategyService} from '../services/strategy.service';
import {Strategy} from '../shared/strategy';
import{baseurl} from '../shared/baseurl';
import{} from 'angular-star-rating';
import{FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  forSellStrategies : Strategy[];
  baseUrl = baseurl;
  searchWord : string;
  wholeStrategies : Strategy[];
  shopForm : FormGroup;
  plan : number = 1;
  avatar : number;
  breakPoint : number = 4;
  rowwHeight : '60vh';
  address : string = baseurl + 'images/static/';
  constructor(private strategyService : StrategyService, private fb : FormBuilder) { }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.avatar  = window.innerWidth * 0.05;
        //this.resize();
  }
  
  ngOnInit() {
    this.strategyService.getForSellStrategies().subscribe((strategies) => {
        this.avatar = window.innerWidth * 0.05;
       // this.resize()
        this.forSellStrategies = strategies;
        this.wholeStrategies = this.forSellStrategies;
        console.log(this.forSellStrategies);
        this.createForm();
    })
  }
  searchThis(){
    this.forSellStrategies=  this.wholeStrategies
    .filter((str) => str.maker.username.includes(this.searchWord) || str.Name.includes(this.searchWord));
  }
  createForm(){
    this.shopForm = this.fb.group({
      rate : ['']
    })
  }
  onValueChanged(data : any){
    if(!this.shopForm){return ;}
    const form = this.shopForm;
  }
  //change strategy price when plan is changed
  onPlanChange(event, Strategy){
    switch (event.value) {
      case 'weekly':
        this.plan = 1;
          break;
      case 'monthly':
        this.plan = 0.15;
          break;  
      case 'threeMonth':
        this.plan = 0.2;
          break;  
      case 'sixMonth':
        this.plan = 0.25;
          break;  
      case 'yearly':
        this.plan = 0.3;
          break;  

      default:
        break;
    }
    Strategy.price = Strategy.price * this.plan;
    this.plan=1;

  }

  purchase(name : string){
    this.strategyService.purchase(name).subscribe((user) => {
      if(user){
        console.log('Purchased Successfully');
      }
    })
  }
}
