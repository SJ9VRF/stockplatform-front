import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import{ QuestionBase } from '../shared/question/question-base';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-question',
  templateUrl: './dynamic-form-question.component.html',
  styleUrls: ['./dynamic-form-question.component.scss']
})
export class DynamicFormQuestionComponent implements OnInit {

  @Input() question : QuestionBase<string | number>;
  @Input() form : FormGroup;
  get isValid() { return this.form.controls[this.question.key].valid; }
  selected : any;
  constructor() { }

  ngOnInit() {
    if(this.question.type=== "number"){
      this.selected = this.question.selected.toString();
    }
    else{
      this.selected = this.question.selected;
    }
    console.log('dynamic-form-question' , this.question, this.selected);
    console.log('dynamic-form-question sele' ,  this.selected);
  }
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      let chng = changes[propName];
      let cur  = chng.currentValue;
      let prev = chng.previousValue;
      console.log('prev',prev);
      console.log('curr',cur);
      if(propName==="form"){
        console.log('formchange',this.form);
      }
    }
  }
}
