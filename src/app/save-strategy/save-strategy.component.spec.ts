import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveStrategyComponent } from './save-strategy.component';

describe('SaveStrategyComponent', () => {
  let component: SaveStrategyComponent;
  let fixture: ComponentFixture<SaveStrategyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveStrategyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveStrategyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
