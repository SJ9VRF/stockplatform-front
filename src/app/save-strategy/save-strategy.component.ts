import { Component, OnInit, Inject } from '@angular/core';
import{MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import{DialogData} from '../shared/dialogData';
@Component({
  selector: 'app-save-strategy',
  templateUrl: './save-strategy.component.html',
  styleUrls: ['./save-strategy.component.scss']
})
export class SaveStrategyComponent implements OnInit {

  constructor(public dialogRef : MatDialogRef<SaveStrategyComponent>,
    @Inject(MAT_DIALOG_DATA) public data : DialogData) { }

  ngOnInit() {
  }
  closeSaveDialog(): void {
    this.dialogRef.close();
  }
}
