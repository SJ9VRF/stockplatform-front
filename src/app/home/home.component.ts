import { Component, OnInit, HostListener } from '@angular/core';
import {baseurl} from '../shared/baseurl';
import{AuthService} from '../services/auth.service';
import{FormGroup, FormBuilder, Validators} from '@angular/forms'
import { Router } from '@angular/router';
import {TranslocoService} from '@ngneat/transloco';

interface Data {
  username : string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  address : string = baseurl + 'images/static/';
  hide : boolean = true;
  message : string='';
  logInForm : FormGroup;
  user = {email : '' , password : ''};
  errMess : string;
  isLoggedIn : boolean = false;
  formErrors = {
      'email' : '',
      'password': ''
    };
    validationMessages = {
      'email' : { 
        'required': 'آدرس ایمیل الزامی است',
        'email' : 'فرمت ایمیل نادرست است'
      },
      'password' : {
        'required' : 'رمز عبور الزامی است'
      }
    }

  homeImageAddress : string = baseurl + 'images/static/Home.jpeg' ;
  homelogoImageAddress : string = baseurl + 'images/static/en-home-logo.jpeg';
  flag : number;
  avatar : number;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.avatar  = window.innerWidth * 0.04;
        this.flag = window.innerWidth * 0.015;
        //this.resize();
  }
  
  //address : string = baseurl + 'images/static/';
  signUpResult : string;
  constructor(private authservice : AuthService, private fb : FormBuilder,
    private router : Router, private translocoService : TranslocoService) { }

  ngOnInit() {

    this.createForm();
    
    this.user = this.logInForm.value;
    this.authservice.loadUserCredentials();
    this.isLoggedIn = this.authservice.isLoggedIn();
    let lang = this.translocoService.getActiveLang();
    if(lang === "en"){
      this.homelogoImageAddress =  baseurl + 'images/static/en-home-logo.jpeg';
    }
    else {
      this.homelogoImageAddress =  baseurl + 'images/static/fa-home-logo.jpeg';
    }

  }
  createForm(){
    this.logInForm = this.fb.group({
       email : ['',[ Validators.required , Validators.email]],
       password : ['' , [Validators.required]]
     });   
     this.logInForm.valueChanges.subscribe((data) => this.onValueChanged(data));
  }
  
  onValueChanged(data : any){
    if(!this.logInForm){return ;}
    const form = this.logInForm;
    //clear previous err message
    for(const field in this.formErrors){
      if(this.formErrors.hasOwnProperty(field)){
        this.formErrors[field] = '';
        const control = form.get(field);
        //re check for erros
        if(control && control.dirty && !control.valid){
          const messages = this.validationMessages[field];
          console.log('control errors : ' ,control.errors);
          for(const key in control.errors){
            if(control.errors.hasOwnProperty(key)){
              console.log('key =' , key);
              console.log('message[key]', messages);
              this.formErrors[field] += this.translocoService.translate(`formErrors.${field}.${key}`) + '\t';
            }
          }
        }
      }
    }
  }
  onSubmitLogInForm(){
    this.user = this.logInForm.value;
    console.log('user',this.user);
    this.authservice.logIn(this.user).subscribe((result) => {
      this.router.navigate(['usersProfile']);
      console.log('result', result);
    }, (err)=>{
      this.errMess = 'رمز عبور یا آدرس ایمیل اشتباه است';
      console.log('err' , err);
    })
  }
  onSubmitSignUpForm(){
    console.log('user first' , this.user);
    this.user = this.logInForm.value;
    this.authservice.signUp(this.user)
    .subscribe((result) => {
        console.log('result signup :' , result);
        this.signUpResult = "ثبت نام با موفقیت انجام شد-می توانید وارد شوید"
    }, (error) => {
      console.log('result signup :' ,error);
    });
}
clickProtected(){
  if(!this.authservice.isLoggedIn()){
    console.log('here');
   this.message = 'برای بازدید از صفحات فروشگاه و ارز دیجیتال ثبت نام/ورود کنید';
  }
  else{
    console.log('there')
    this.message ='';
  }
}
changeLang(lang : string){
  this.translocoService.setActiveLang(lang);
  if(lang === "en"){
    this.homelogoImageAddress =  baseurl + 'images/static/en-home-logo.jpeg';
  }
  else {
    this.homelogoImageAddress =  baseurl + 'images/static/fa-home-logo.jpeg';
  }
}
}
